import React, { useEffect, useState } from "react";
import { Button, Grid, LinearProgress, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { resetDefault } from "../redux/actions/resetDefault";
import { updateCompany } from "../redux/actions/Company/updateCompany";
import { getCompany } from "../redux/actions/Company/getCompany";
import { connect } from "react-redux";
import * as R from "ramda";
import { Field, Form, Formik } from "formik";
import Collapse from "@material-ui/core/Collapse/Collapse";
import Alert from "@material-ui/lab/Alert/Alert";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import { TextField } from "formik-material-ui";
import CircularProgress from "@material-ui/core/CircularProgress";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    maxWidth: 610,
    paddingTop: 8,
  },
  DocsContainer: {
    display: "flex",
    flexDirection: "column",
    borderRadius: 2,
    minHeight: 22,
    border: "1px solid rgba(0, 0, 0, 0.12)",
    width: "100%",
    marginTop: 16,
    position: "relative",
  },
  ImgContainer: {
    maxHeight: 440,
  },
  LabelAbsolute: {
    position: "absolute",
    top: -16,
    left: 16,
    padding: 4,
    backgroundColor: "#fafafa",
  },
});
const Company = ({ classes, company, companyL, updating, ...props }) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);

  useEffect(() => {
    props.getCompany();
  }, []);

  useEffect(() => {
    if (formValues !== null && company.id) {
      props.updateCompany(company.id, formValues);
    }
  }, [formValues]);

  useEffect(() => {
    if (updating === false) {
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
        setResetAlert(true);
      }, 750);
    }
    return clearTimeout();
  }, [updating]);

  return (
    <div className={classes.Container}>
      {companyL === false ? (
        <>
          <Typography variant="h4">Компания</Typography>
          <Grid
            container
            justify="flex-start"
            alignItems="center"
            direction="row"
            spacing={2}
            style={{ paddingLeft: 8, marginTop: 16 }}
          >
            <Formik
              initialValues={{
                name: company.name,
                inn: company.inn,
                kpp: company.kpp,
                address: company.address,
                contract: company.contract,
                contract_person: company.contract_person,
                contract_foundation: company.contract_foundation,
                contract_position: company.contract_position,
              }}
              // validate={(values) => {
              //   const errors = {};
              //   if (!values.email) {
              //     errors.email = "Обязательное поле";
              //   } else if (
              //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
              //       values.email
              //     )
              //   ) {
              //     errors.email = "Invalid email address";
              //   }
              //   return errors;
              // }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  setSubmitting(false);
                  setFormValues(values);
                }, 500);
              }}
            >
              {({ submitForm, isSubmitting }) => (
                <Form>
                  <Grid
                    container
                    justify="flex-end"
                    direction="column"
                    style={{ marginTop: "16px", marginLeft: 8 }}
                  >
                    <Grid container direction="column">
                      <Grid item sm={12}>
                        {console.log("company", company)}
                        <Field
                          name="name"
                          label="Название"
                          type="text"
                          fullWidth
                          component={TextField}
                        />
                      </Grid>
                      <Grid container spacing={2} style={{ marginTop: 16 }}>
                        <Grid item xs={12} sm={6}>
                          <Field
                            name="inn"
                            label="ИНН"
                            type="text"
                            fullWidth
                            component={TextField}
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <Field
                            name="kpp"
                            label="КПП"
                            type="text"
                            fullWidth
                            component={TextField}
                          />
                        </Grid>
                      </Grid>
                      <Grid item sm={12} style={{ marginTop: "16px" }}>
                        <Field
                          name="address"
                          label="Адрес"
                          type="text"
                          fullWidth
                          component={TextField}
                        />
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      justify="flex-end"
                      style={{ marginTop: "16px", maxWidth: 618 }}
                    >
                      <div className={classes.DocsContainer}>
                        <div className={classes.LabelAbsolute}>Документы</div>
                        <Grid item sm={12} style={{ padding: "16px" }}>
                          <Field
                            name="contract_position"
                            label="Должность"
                            type="text"
                            fullWidth
                            component={TextField}
                          />
                        </Grid>
                        <Grid item sm={12} style={{ padding: "16px" }}>
                          <Field
                            name="contract_person"
                            label="ФИО"
                            type="text"
                            fullWidth
                            component={TextField}
                          />
                        </Grid>
                        <Grid item sm={12} style={{ padding: "16px" }}>
                          <Field
                            name="contract_foundation"
                            label="Основание"
                            type="text"
                            fullWidth
                            component={TextField}
                          />
                        </Grid>{" "}

                      </div>
                    </Grid>
                    {isSubmitting && <LinearProgress />}

                    <Collapse
                      in={open}
                      style={{ marginTop: "16px", maxWidth: "100%" }}
                    >
                      <Alert
                        severity="success"
                        action={
                          <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                              setOpen(false);
                            }}
                          >
                            <CloseIcon fontSize="inherit" color="primary" />
                          </IconButton>
                        }
                      >
                        Данные изменены
                      </Alert>
                    </Collapse>
                    <div style={{ display: "flex", alignSelf: "flex-end" }}>
                      <Button
                        variant="contained"
                        color="primary"
                        style={{ textTransform: "none" }}
                        disabled={isSubmitting}
                        onClick={submitForm}
                      >
                        Сохранить
                      </Button>
                    </div>
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  company: state.getCompanyR.company,
  companyL: state.getCompanyR.companyL,
  updating: state.updateCompanyR.updating,
});

export default R.compose(
  connect(mapStateToProps, {
    getCompany,
    updateCompany,
    resetDefault,
  }),
  withStyles(styles)
)(Company);
