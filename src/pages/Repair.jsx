import React, { useState } from "react";
import { Button, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import RepairTable from "../components/RepairTable/RepairTable";
import { useHistory } from "react-router";
const styles = theme => ({
  margin: {
    margin: theme.spacing.unit * 1
  },
  padding: {
    padding: theme.spacing.unit
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh"
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8
  },
  input: {
    display: "none"
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "flex-end",
    marginTop: 16
  },
  ButtonGroupFooter: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "flex-end",
    marginTop: 16
  }
});
const Repair = ({ classes }) => {
  const [tableStatus, setTableStatus] = useState(false);
  let history = useHistory();

  const handleDrop = event => {
    console.log("event.target.file[0]", event.target.files);
    if (event.target.files !== undefined) {
      setTableStatus(true);
    }
  };

  return (
    <div className={classes.Container}>
      <Typography variant="h4" style={{ marginBottom: 16 }}>
        В ремонт
      </Typography>
      <div>
        <RepairTable tableStatus={tableStatus} />
      </div>
      <div className={classes.ButtonGroupFooter}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/detailPage")}
          style={{ marginRight: 16 }}
        >
          Создать заявку
        </Button>
        <input
          accept="image/*"
          className={classes.input}
          id="contained-button-file"
          multiple
          type="file"
          onChange={handleDrop}
        />
        <label htmlFor="contained-button-file">
          <Button
            variant="contained"
            color="primary"
            component="span"

          >
            Загрузить из файла
          </Button>
        </label>
      </div>
    </div>
  );
};

export default withStyles(styles)(Repair);
