import React, { useEffect, useState } from "react";
import {
  Paper,
  withStyles,
  Grid,
  Button,
  LinearProgress,
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";

import { useHistory } from "react-router";
import Alert from "@material-ui/lab/Alert";
import Typography from "@material-ui/core/Typography";
import * as R from "ramda";
import { connect } from "react-redux";
import { Face } from "@material-ui/icons";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { resetPassword } from "../redux/auth/resetPassword";
import { SubmitInput } from "../components/SubmitInput/SubmitInput";
import { resetDefault } from "../redux/actions/resetDefault";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  MainForm: {
    display: "flex",
    padding: theme.spacing.unit * 2,
    maxWidth: 440,
  },
});

const ResetPassword = ({
  isReset,
  resetEmailData,
  classes,
  errorMessage,
  ...props
}) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);
  const history = useHistory();
  useEffect(() => {
    if (formValues !== null) {
      console.log("formValues", formValues);
      props.resetPassword(formValues.email);
    }
  }, [formValues]);
  useEffect(() => {
    if (isReset) {
      history.push("/");
    }
  }, [resetEmailData, isReset]);
  useEffect(() => {
    if (errorMessage !== "") {
      setOpen(true);
    }
  }, [errorMessage]);
  useEffect(() => {
    if (ResetAlert === true) {
      props.resetDefault();
    }
  }, [ResetAlert]);
  return (
    <div className={classes.FormC}>
      <Paper className={classes.MainForm}>
        <div className={classes.margin}>
          <Formik
            initialValues={{
              email: "",
            }}
            validate={(values) => {
              const errors = {};
              if (!values.email) {
                errors.email = "Обязательное поле";
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
              ) {
                errors.email = "Invalid email address";
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);
                setFormValues(values);
              }, 500);
            }}
          >
            {({ submitForm, isSubmitting }) => (
              <Form>
                <Grid
                  item
                  style={{
                    display: "flex",
                    textAlign: "center",
                    justifyContent: "center",
                    marginBottom: 16,
                  }}
                >
                  <Typography>Восстановление пароля</Typography>
                </Grid>

                <Grid
                  container
                  spacing={2}
                  alignItems="center"
                  justify="center"
                >
                  <Grid item>
                    <Face />
                  </Grid>
                  <Grid item md={true} sm={true} xs={true}>
                    <Field
                      component={SubmitInput}
                      name="email"
                      type="email"
                      label="Email"
                      fullWidth
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  justify="flex-end"
                  direction="column"
                  style={{ marginTop: "16px" }}
                >
                  {isSubmitting && <LinearProgress />}
                  <Collapse
                    in={open}
                    style={{ marginTop: "16px", maxWidth: 230 }}
                  >
                    <Alert
                      severity="error"
                      action={
                        <IconButton
                          aria-label="close"
                          color="inherit"
                          size="small"
                          onClick={() => {
                            setOpen(false);
                            setResetAlert(true);
                          }}
                        >
                          <CloseIcon fontSize="inherit" />
                        </IconButton>
                      }
                    >
                      {errorMessage}
                    </Alert>
                  </Collapse>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={isSubmitting}
                    onClick={submitForm}
                  >
                    Сбросить пароль
                  </Button>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Paper>
    </div>
  );
};
const mapStateToProps = (state) => ({
  isReset: state.resetPasswordR.isReset,
  resetEmailData: state.resetPasswordR.resetEmailData,
  errorMessage: state.resetPasswordR.errorMessage,
});

export default R.compose(
  connect(mapStateToProps, {
    resetPassword,
    resetDefault,
  }),
  withStyles(styles)
)(ResetPassword);
