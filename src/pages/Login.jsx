import React, { useEffect, useState } from "react";
import {
  Paper,
  withStyles,
  Grid,
  Button,
  LinearProgress,
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import { useHistory } from "react-router";
import Alert from "@material-ui/lab/Alert";
import Typography from "@material-ui/core/Typography";
import * as R from "ramda";
import { connect } from "react-redux";
import { login } from "../redux/auth/login";
import { Face, Fingerprint } from "@material-ui/icons";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Link from "@material-ui/core/Link";
import { SubmitInput } from "../components/SubmitInput/SubmitInput";
import { resetDefault } from "../redux/actions/resetDefault";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  MainForm: {
    display: "flex",
    padding: theme.spacing.unit * 2,
    maxWidth: 440,
  },
});

const Login = ({ isAuth, userData, classes, errorMessage, ...props }) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);
  const history = useHistory();
  useEffect(() => {
    if (formValues !== null) {
      console.log("formValues", formValues);
      props.login(formValues.identifier, formValues.password);
    }
  }, [formValues]);
  useEffect(() => {
    if (ResetAlert === true) {
      props.resetDefault();
    }
  }, [ResetAlert]);
  useEffect(() => {
    if (errorMessage !== "") {
      setOpen(true);
    }
  }, [errorMessage]);
  useEffect(() => {
    if (isAuth && (userData.jwt !== null || userData.jwt !== "")) {
      localStorage.setItem("userInfo", userData.jwt);
      history.push("/storehouses");
      window.location.reload();
    }
  }, [userData, isAuth]);
  return (
    <div className={classes.FormC}>
      <Paper className={classes.MainForm}>
        <div className={classes.margin}>
          <Formik
            initialValues={{
              identifier: "",
              password: "",
            }}
            validate={(values) => {
              const errors = {};
              if (!values.identifier) {
                errors.identifier = "Обязательное поле";
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
                  values.identifier
                )
              ) {
                errors.identifier = "Invalid email address";
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);
                setFormValues(values);
              }, 500);
            }}
          >
            {({ submitForm, isSubmitting }) => (
              <Form>
                <Grid
                  item
                  style={{
                    display: "flex",
                    textAlign: "center",
                    justifyContent: "center",
                    marginBottom: 16,
                  }}
                >
                  <Typography>Вход</Typography>
                </Grid>

                <Grid container spacing={2} alignItems="flex-end">
                  <Grid item>
                    <Face />
                  </Grid>
                  <Grid item md={true} sm={true} xs={true}>
                    <Field
                      component={TextField}
                      name="identifier"
                      type="email"
                      label="Email"
                      fullWidth
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={2} alignItems="flex-end">
                  <Grid item>
                    <Fingerprint />
                  </Grid>
                  <Grid item md={true} sm={true} xs={true}>
                    <Field
                      component={SubmitInput}
                      type="password"
                      label="Password"
                      name="password"
                      fullWidth
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  justify="flex-end"
                  direction="column"
                  style={{ marginTop: "16px" }}
                >
                  {isSubmitting && <LinearProgress />}

                  <Collapse
                    in={open}
                    style={{ marginTop: "16px", maxWidth: 230 }}
                  >
                    <Alert
                      severity="error"
                      action={
                        <IconButton
                          aria-label="close"
                          color="inherit"
                          size="small"
                          onClick={() => {
                            setOpen(false);
                            setResetAlert(true);
                          }}
                        >
                          <CloseIcon fontSize="inherit" />
                        </IconButton>
                      }
                    >
                      {errorMessage}
                    </Alert>
                  </Collapse>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={isSubmitting}
                    onClick={submitForm}
                  >
                    Вход
                  </Button>
                </Grid>
                <Grid
                  item
                  style={{
                    display: "flex",
                    textAlign: "center",
                    justifyContent: "center",
                    marginTop: "16px",
                  }}
                >
                  <Typography>
                    <Link
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        history.push("/reset");
                      }}
                    >
                      Забыли пароль?
                    </Link>
                  </Typography>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Paper>
    </div>
  );
};
const mapStateToProps = (state) => ({
  isAuth: state.AuthReducer.isAuth,
  userData: state.AuthReducer.userData,
  errorMessage: state.AuthReducer.errorMessage,
});

export default R.compose(
  connect(mapStateToProps, {
    login,
    resetDefault,
  }),
  withStyles(styles)
)(Login);
