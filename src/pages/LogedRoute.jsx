import React from "react";
import { Route, Redirect } from "react-router";

const LogedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.userInfo === undefined || "" ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/storehouses",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

export default LogedRoute;
