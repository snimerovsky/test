import * as React from "react";
import { Formik, Form, Field } from "formik";
import {Button, Grid, LinearProgress, withStyles} from "@material-ui/core";
import { TextField } from "formik-material-ui";
import * as R from "ramda";
import { connect } from "react-redux";
import { login } from "../redux/auth/login";
import { useState } from "react";
import { useHistory } from "react-router";
import { useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import {Face, Fingerprint} from "@material-ui/icons";

const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  MainForm: {
    display: "flex",
    padding: theme.spacing.unit * 2,
    maxWidth: 440,
  },
});

const AuthLogin = ({ isAuth, userData, classes, ...props }) => {
  const [formValues, setFormValues] = useState(null);
  const history = useHistory();
  useEffect(() => {
    if (formValues !== null) {
      console.log("formValues", formValues);
      props.login(formValues.identifier, formValues.password);
    }
  }, [formValues]);
  useEffect(() => {
    if (
      isAuth &&
      (userData.data.token !== null || userData.data.token !== "")
    ) {
      localStorage.setItem("userInfo", userData.data.token);
      history.push("/storehouses");
      // window.location.reload();
    }
  }, [userData, isAuth]);
  return (
    <Formik
      initialValues={{
        identifier: "",
        password: "",
      }}
      validate={(values) => {
        const errors = {};
        if (!values.identifier) {
          errors.identifier = "Required";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.identifier)
        ) {
          errors.identifier = "Invalid email address";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          setSubmitting(false);
          setFormValues(values);
        }, 500);
      }}
    >
      {({ submitForm, isSubmitting }) => (
          <Form>
              <Grid
                  item
                  style={{
                      display: "flex",
                      textAlign: "center",
                      justifyContent: "center",
                      marginBottom: 16,
                  }}
              >
                  <Typography>Вход</Typography>
              </Grid>

              <Grid container spacing={2} alignItems="flex-end">
                  <Grid item>
                      <Face />
                  </Grid>
                  <Grid item md={true} sm={true} xs={true}>
                      <Field
                          component={TextField}
                          name="identifier"
                          type="email"
                          label="Email"
                          fullWidth
                      />
                  </Grid>
              </Grid>
              <Grid container spacing={2} alignItems="flex-end">
                  <Grid item>
                      <Fingerprint />
                  </Grid>
                  <Grid item md={true} sm={true} xs={true}>
                      <Field
                          component={TextField}
                          type="password"
                          label="Password"
                          name="password"
                          fullWidth
                      />
                  </Grid>
              </Grid>
              <Grid
                  container
                  justify="flex-end"
                  style={{ marginTop: "16px" }}
              >
                  {isSubmitting && <LinearProgress />}
                  <Button
                      variant="contained"
                      color="primary"
                      disabled={isSubmitting}
                      onClick={submitForm}
                  >
                      Submit
                  </Button>
              </Grid>
          </Form>
      )}
    </Formik>
  );
};
const mapStateToProps = (state) => ({
  isAuth: state.AuthReducer.isAuth,
  userData: state.AuthReducer.userData,
  errorMessage: state.AuthReducer.errorMessage,
});

export default R.compose(
  connect(mapStateToProps, {
    login,
  }),
  withStyles(styles)
)(AuthLogin);
