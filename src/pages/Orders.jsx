import React from "react";
import { Button, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router";
import OrdersTable from "../components/OrdersTable/OrdersTable";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  input: {
    display: "none",
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 16,
  },
});
const Orders = ({ classes }) => {
  let history = useHistory();
  return (
    <div className={classes.Container}>
      <div className={classes.ButtonGroup}>
        <Typography variant="h4">Заявки</Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/createTicket")}
        >
          Добавить
        </Button>
      </div>
      <div>
        <OrdersTable />
      </div>
    </div>
  );
};

export default withStyles(styles)(Orders);
