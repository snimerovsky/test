import React, { useEffect, useState } from "react";
import { Button, Grid, LinearProgress, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import * as R from "ramda";
import { connect } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import Collapse from "@material-ui/core/Collapse/Collapse";
import Alert from "@material-ui/lab/Alert/Alert";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";

import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import {useHistory, useLocation, useParams} from "react-router";
import { updateProfile } from "../../redux/actions/Profile/updateProfile";
import { getUserId } from "../../redux/actions/Users/getUserId";
import { resetDefault } from "../../redux/actions/resetDefault";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
});
const EditUsers = ({ classes, user, userL, updating, ...props }) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);
  
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    if (id !== undefined) {
      props.getUserId(id);
    }
  }, [id]);
  useEffect(() => {
    if (updating === false) {
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
        setResetAlert(true);
      }, 750);
    }
  }, [updating]);

  useEffect(() => {
    if (formValues !== null && id) {
      props.updateProfile(id, formValues);
    }
  }, [formValues]);
  useEffect(() => {
    if (ResetAlert === true) {
      props.resetDefault();
      history.push("/users");
    }
  }, [ResetAlert]);
  return (
    <div className={classes.Container}>
      {userL === false ? (
        <>
          <Typography variant="h4">Профиль</Typography>
          <Grid
            container
            justify="flex-start"
            alignItems="center"
            direction="row"
            spacing={2}
            style={{ marginTop: 16, paddingLeft: 16 }}
          >
            <div
              style={{
                height: 320,
                width: 320,
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "space-between",
              }}
            >
              <Formik
                initialValues={{
                  email: user.email,
                  name_first: user.name_first,
                  name_last: user.name_last,
                  name_middle: user.name_middle,
                }}
                // validate={(values) => {
                //   const errors = {};
                //   if (!values.email) {
                //     errors.email = "Обязательное поле";
                //   } else if (
                //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
                //       values.email
                //     )
                //   ) {
                //     errors.email = "Invalid email address";
                //   }
                //   return errors;
                // }}
                onSubmit={(values, { setSubmitting }) => {
                  setTimeout(() => {
                    setSubmitting(false);
                    setFormValues(values);
                  }, 500);
                }}
              >
                {({ submitForm, isSubmitting }) => (
                  <Form>
                    <Grid container spacing={2} alignItems="flex-end">
                      <Field
                        component={TextField}
                        name="name_last"
                        type="text"
                        label="Фамилия"
                        fullWidth
                      />
                      <Field
                        component={TextField}
                        name="name_first"
                        type="text"
                        label="Имя"
                        fullWidth
                        style={{ marginTop: "16px" }}
                      />
                      <Field
                        component={TextField}
                        name="name_middle"
                        type="text"
                        label="Отчество"
                        fullWidth
                        style={{ marginTop: "16px" }}
                      />
                      <Field
                        component={TextField}
                        name="email"
                        type="email"
                        label="Email"
                        fullWidth
                        style={{ marginTop: "16px" }}
                      />
                        <Field
                          name="password"
                          label="Пароль"
                          type="text"
                          fullWidth
                          component={TextField}
                          style={{ marginTop: "16px" }}
                        />
                    </Grid>
                    <Grid
                      container
                      justify="flex-end"
                      direction="column"
                      style={{ marginTop: "16px", marginLeft: 8 }}
                    >
                      {isSubmitting && <LinearProgress />}

                      <Collapse
                        in={open}
                        style={{ marginTop: "16px", maxWidth: "100%" }}
                      >
                        <Alert
                          severity="success"
                          action={
                            <IconButton
                              aria-label="close"
                              color="inherit"
                              size="small"
                              onClick={() => {
                                setOpen(false);
                              }}
                            >
                              <CloseIcon fontSize="inherit" color="primary" />
                            </IconButton>
                          }
                        >
                          Данные изменены
                        </Alert>
                      </Collapse>
                      <div style={{ display: "flex", alignSelf: "flex-end" }}>
                        <Button
                          variant="contained"
                          color="primary"
                          style={{ textTransform: "none" }}
                          disabled={isSubmitting}
                          onClick={submitForm}
                        >
                          Сохранить
                        </Button>
                      </div>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          </Grid>
        </>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};
const mapStateToProps = (state) => ({
  user: state.getUserIdR.user,
  userL: state.getUserIdR.userL,
  updating: state.updateProfileR.updating,
});

export default R.compose(
  connect(mapStateToProps, {
    getUserId,
    updateProfile,
    resetDefault,
  }),
  withStyles(styles)
)(EditUsers);
