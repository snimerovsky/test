import React, { useEffect, useState } from "react";
import { Button, Grid, LinearProgress, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import * as R from "ramda";
import { Field, Form, Formik } from "formik";
import Collapse from "@material-ui/core/Collapse/Collapse";
import Alert from "@material-ui/lab/Alert/Alert";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import { useHistory } from "react-router";
import { resetDefault } from "../../redux/actions/resetDefault";
import { TextField } from "formik-material-ui";
import { createUsers } from "../../redux/actions/Users/createUsers";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    maxWidth: 610,
    paddingTop: 8,
  },
  DocsContainer: {
    display: "flex",
    flexDirection: "column",
    borderRadius: 2,
    minHeight: 22,
    border: "1px solid rgba(0, 0, 0, 0.12)",
    width: "100%",
    marginTop: 16,
    position: "relative",
  },
  ImgContainer: {
    maxHeight: 440,
  },
  LabelAbsolute: {
    position: "absolute",
    top: -16,
    left: 16,
    padding: 4,
    backgroundColor: "#fafafa",
  },
});
const CreateUsers = ({ classes, creating, ...props }) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);
  const history = useHistory();

  useEffect(() => {
    if (creating === false) {
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
        setResetAlert(true);
      }, 750);
    }
    return clearTimeout();
  }, [creating]);

  useEffect(() => {
    if (formValues !== null) {
      props.createUsers(formValues);
    }
  }, [formValues]);
  useEffect(() => {
    if (ResetAlert === true) {
      props.resetDefault();
      history.push("/users");
    }
  }, [ResetAlert]);
  return (
    <div className={classes.Container}>
      <>
        <Typography variant="h4">Создание Пользователя</Typography>

        <Grid
          container
          justify="flex-start"
          alignItems="center"
          direction="row"
          spacing={2}
          style={{ paddingLeft: 8, marginTop: 16 }}
        >
          <Formik
            initialValues={{
              email: "",
              name_first: "",
              name_last: "",
              name_middle: "",
              username: "",
            }}
            // validate={(values) => {
            //   const errors = {};
            //   if (!values.email) {
            //     errors.email = "Обязательное поле";
            //   } else if (
            //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
            //       values.email
            //     )
            //   ) {
            //     errors.email = "Invalid email address";
            //   }
            //   return errors;
            // }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);
                setFormValues(values);
              }, 500);
            }}
          >
            {({ submitForm, isSubmitting }) => (
              <Form>
                <Grid
                  container
                  justify="flex-end"
                  direction="column"
                  style={{ marginTop: "16px", marginLeft: 8, maxWidth: 360 }}
                >
                  <Grid container spacing={2} alignItems="flex-end">
                    <Field
                      component={TextField}
                      name="name_last"
                      type="text"
                      label="Фамилия"
                      fullWidth
                    />
                    <Field
                      component={TextField}
                      name="name_first"
                      type="text"
                      label="Имя"
                      fullWidth
                      style={{ marginTop: "16px" }}
                    />
                    <Field
                      component={TextField}
                      name="name_middle"
                      type="text"
                      label="Отчество"
                      fullWidth
                      style={{ marginTop: "16px" }}
                    />
                    <Field
                      component={TextField}
                      name="email"
                      type="email"
                      label="Email"
                      fullWidth
                      style={{ marginTop: "16px" }}
                    />
                    <Field
                      component={TextField}
                      name="username"
                      type="text"
                      label="Username"
                      fullWidth
                      style={{ marginTop: "16px" }}
                    />
                    <Field
                      name="password"
                      label="Пароль"
                      type="text"
                      fullWidth
                      component={TextField}
                      style={{ marginTop: "16px" }}
                    />
                  </Grid>
                  <Grid
                    container
                    justify="flex-end"
                    direction="column"
                    style={{ marginTop: "16px", marginLeft: 8 }}
                  >
                    {isSubmitting && <LinearProgress />}

                    <Collapse
                      in={open}
                      style={{ marginTop: "16px", maxWidth: "100%" }}
                    >
                      <Alert
                        severity="success"
                        action={
                          <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                              setOpen(false);
                            }}
                          >
                            <CloseIcon fontSize="inherit" color="primary" />
                          </IconButton>
                        }
                      >
                        Данные изменены
                      </Alert>
                    </Collapse>
                    <div style={{ display: "flex", alignSelf: "flex-end" }}>
                      <Button
                        variant="contained"
                        color="primary"
                        style={{ textTransform: "none" }}
                        disabled={isSubmitting}
                        onClick={submitForm}
                      >
                        Сохранить
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        </Grid>
      </>
    </div>
  );
};

const mapStateToProps = (state) => ({
  creating: state.createUsersR.creating,
});

export default R.compose(
  connect(mapStateToProps, {
    createUsers,
    resetDefault,
  }),
  withStyles(styles)
)(CreateUsers);
