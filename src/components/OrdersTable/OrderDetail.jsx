import React, { useState } from "react";
import { Grid, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Paper from "@material-ui/core/Paper";
import Input from "@material-ui/core/Input";
import DateFnsUtils from "@date-io/date-fns";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from "@material-ui/pickers";
import { useSelector } from "react-redux";
import DetailTable from "../DetailTable/DetailTable";
import StepperItem from "../Stepper";

const styles = (theme) => ({
    margin: {
        margin: theme.spacing.unit * 1,
    },
    padding: {
        padding: theme.spacing.unit,
    },
    FormC: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
    },
    Container: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing.unit * 2,
        paddingTop: 8,
        maxWidth: 1440,
    },
    ImgContainer: {
        maxHeight: 440,
    },
    HorizontalInput: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
    },
    InputCard: {
        padding: 16,
        minHeight: 430,
        [theme.breakpoints.down("sm")]: {
            minHeight: 140,
            height: "auto",
        },
    },
});
const OrderDetail = ({ classes }) => {
    const [selectedDate, handleDateChange] = useState(new Date());

    return (
        <div className={classes.Container}>
            <Typography variant="h4">Заявка 000146</Typography>
            <Grid
                container
                justify="space-between"
                alignItems="flex-start"
                direction="row"
                spacing={2}
                style={{ marginTop: 16 }}
            >
                <Grid item xs={12} md={6} lg={4}>
                    <Paper className={classes.InputCard}>
                        <FormControl className={classes.HorizontalInput}>
                            <span>Клиент: </span>
                            <Input
                                id="client"
                                aria-describedby="my-helper-text"
                                defaultValue="ФГК"
                                style={{ width: "100%", margin: 16 }}
                            />
                        </FormControl>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <div className={classes.HorizontalInput}>
                                <span>Дата: </span>
                                <KeyboardDatePicker
                                    variant="inline"
                                    value={selectedDate}
                                    placeholder="13/03/2020"
                                    onChange={(date) => handleDateChange(date)}
                                    format="dd/MM/yyyy"
                                    style={{ width: "100%", margin: 16, marginLeft: 28 }}
                                />
                            </div>
                        </MuiPickersUtilsProvider>
                        <FormControl className={classes.HorizontalInput}>
                            <span>Статус: </span>
                            <Input
                                id="status"
                                aria-describedby="my-helper-text"
                                defaultValue="новая"
                                style={{ width: "100%", margin: 16 }}
                            />
                        </FormControl>
                    </Paper>
                </Grid>
                <Grid item xs={12} md={6} lg={8}>
                    <DetailTable Rows={[]}/>
                </Grid>
                <StepperItem />
            </Grid>
        </div>
    );
};

export default withStyles(styles)(OrderDetail);
