import React, { useEffect, useState } from "react";
import { Grid, withStyles, Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Paper from "@material-ui/core/Paper";
import Input from "@material-ui/core/Input";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { useDispatch, useSelector } from "react-redux";
import DetailTableOrder from "../DetailTable/DetailTableOrder";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import DescriptionIcon from "@material-ui/icons/Description";
import Link from "@material-ui/core/Link";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import { useHistory } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";

import StepLabel from "@material-ui/core/StepLabel";
import { getOrdersProducts } from "../../redux/actions/Orders/getOrdersProducts";
import { getStatuses } from "../../redux/actions/Orders/getStatuses";
import { getDocs } from "../../redux/actions/Orders/getDocs";
import { createOrders } from "../../redux/actions/Orders/createOrders";
import { getProductTypes } from "../../redux/actions/ProductTypes/getProductTypes";
import { getStoreHouses } from "../../redux/actions/StoreHouses/getStoreHouses";
import { getCompanyId } from "../../redux/actions/Company/getCompanyId";

const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
    maxWidth: 1440,
  },
  ImgContainer: {
    maxHeight: 440,
  },
  HorizontalInput: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  InputCard: {
    padding: 16,
    minHeight: 430,
    [theme.breakpoints.down("sm")]: {
      minHeight: 140,
      height: "auto",
    },
  },
});

const OrderCreate = ({ classes }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [selectedDate, handleDateChange] = useState(new Date());
  const [client, setClient] = useState(-1);
  const [open, setOpen] = useState(false);
  const [dataTable, setDataTable] = useState([]);
  const { Statuses, StatusesL } = useSelector((state) => state.getStatusesR);
  const { Docs, DocsL } = useSelector((state) => state.getDocsR);
  const { profile } = useSelector((state) => state.getProfileR);
  const { company } = useSelector((state) => state.getCompanyIdR);
  const [isGetCompany, setIsGetCompany] = useState(true);
  if (profile !== undefined && isGetCompany) {
    dispatch(getCompanyId(profile.company));
    setIsGetCompany(false);
  }
  if (company.name !== undefined && client === -1) {
    setClient(company.name);
  }
  const [activeStep, setActiveStep] = React.useState(0);
  const { ProductTypes } = useSelector((state) => state.getProductTypesR);
  const { StoreHouses } = useSelector((state) => state.getStoreHousesR);
  const [skipped, setSkipped] = React.useState(new Set());
  const getSteps = () => {
    let names = [];
    for (let i in Statuses) {
      names.push(Statuses[i].name);
    }
    return names;
  };
  const steps = getSteps();
  const { OrdersProducts, OrdersProductsL } = useSelector(
    (state) => state.getOrdersProductsR
  );

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const createOrder = async () => {
    setOpen(true);
    const tickets = [];
    for (let i in dataTable) {
      console.log("dataTable", dataTable[i]);
      tickets.push({
        storehouse: { id: parseInt(dataTable[i].storehouse) },
        product_type: { id: parseInt(dataTable[i].product_type) },
        number: dataTable[i].number,
      });
    }
    const body = {
      status: { id: Statuses[0].id },
      TicketItems: tickets,
    };
    await dispatch(createOrders(body));
    setOpen(false);
    history.push("/tickets");
    window.location.reload();
  };

  useEffect(() => {
    dispatch(getOrdersProducts());
    dispatch(getStatuses());
    dispatch(getDocs());
    dispatch(getProductTypes());
    dispatch(getStoreHouses());
  }, [dispatch]);
  return !StatusesL && !OrdersProductsL && !DocsL ? (
    <>
      {!open ? (
        <div className={classes.Container}>
          <Typography variant="h4">Создание заявки</Typography>
          <Grid
            container
            justify="space-between"
            alignItems="flex-start"
            direction="row"
            spacing={2}
            style={{ marginTop: 16 }}
          >
            <Grid item xs={12} md={6} lg={4}>
              <Paper className={classes.InputCard}>
                <FormControl className={classes.HorizontalInput}>
                  <span>Клиент: </span>
                  <Input
                    id="client"
                    aria-describedby="my-helper-text"
                    value={client}
                    onChange={(e) => setClient(e.target.value)}
                    style={{ width: "100%", margin: 16 }}
                  />
                </FormControl>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <div className={classes.HorizontalInput}>
                    <span>Дата: </span>
                    <KeyboardDatePicker
                      variant="inline"
                      value={selectedDate}
                      placeholder="13/03/2020"
                      onChange={(date) => handleDateChange(date)}
                      format="dd/MM/yyyy"
                      style={{ width: "100%", margin: 16, marginLeft: 28 }}
                    />
                  </div>
                </MuiPickersUtilsProvider>

                <FormControl className={classes.HorizontalInput}>
                  <span>Статус: </span>
                  <Select
                    id="status"
                    aria-describedby="my-helper-text"
                    value={Statuses[0].id}
                    style={{ width: "100%", margin: 16, marginLeft: 28 }}
                  >
                    <MenuItem key={Statuses[0].id} value={Statuses[0].id}>
                      {Statuses[0].name}
                    </MenuItem>
                  </Select>
                </FormControl>
              </Paper>
            </Grid>
            <Grid item xs={12} md={6} lg={8}>
              {StoreHouses.length !== undefined &&
              ProductTypes.length !== undefined ? (
                <DetailTableOrder
                  StoreHousesData={StoreHouses}
                  ProductTypesData={ProductTypes}
                  changeTableData={(e) => setDataTable(e)}
                />
              ) : (
                <CircularProgress />
              )}
            </Grid>
            <Stepper
              activeStep={activeStep}
              style={{ width: "100%", maxWidth: 1440, margin: 8 }}
            >
              {steps.map((label, index) => {
                const stepProps = {};
                const labelProps = {};
                if (isStepSkipped(index)) {
                  stepProps.completed = false;
                }
                return (
                  <Step key={label} {...stepProps}>
                    <StepLabel {...labelProps}>{label}</StepLabel>
                  </Step>
                );
              })}
            </Stepper>
            <Paper style={{ width: "100%", marginTop: 16 }}>
              <Grid container xs={12}>
                <div>
                  <Typography
                    variant="h5"
                    style={{ width: "100%", margin: 16 }}
                  >
                    Документы
                  </Typography>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    {Docs.map((docs) => (
                      <>
                        <DescriptionIcon style={{ margin: 16 }} />
                        <Link>{docs.name}</Link>
                      </>
                    ))}
                  </div>
                </div>
              </Grid>
            </Paper>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                marginTop: "16px",
                width: "100%",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                style={{ textTransform: "none" }}
                onClick={() => createOrder()}
              >
                Сохранить
              </Button>
            </div>
          </Grid>
        </div>
      ) : (
        <CircularProgress />
      )}
    </>
  ) : null;
};

export default withStyles(styles)(OrderCreate);
