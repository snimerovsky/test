import {
  Grid,
  PagingPanel,
  Table,
  TableHeaderRow,
  TableSelection,
} from "@devexpress/dx-react-grid-material-ui";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";

import {
  IntegratedPaging,
  IntegratedSelection,
  IntegratedSorting,
  PagingState,
  SelectionState,
  SortingState,
} from "@devexpress/dx-react-grid";
import { useHistory } from "react-router";
import IconButton from "@material-ui/core/IconButton";
import * as R from "ramda";
import { connect } from "react-redux";
import { saveRows } from "../../redux/actions/saveRows";
import { withStyles } from "@material-ui/core";
import DeleteDialog from "../DeleteDialog/DeleteDialog";
import {
  Cell,
  TableComponent,
} from "../CustomTableComponents/CustomTableComponents";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getOrders } from "../../redux/actions/Orders/getOrders";
import { deleteOrders } from "../../redux/actions/Orders/deleteOrders";

const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "row",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "space-between",
    alignItems: "center",
  },
  ButtonGroupFooter: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "flex-end",
    marginTop: 16,
  },
  input: {
    display: "none",
  },
});

const OrdersTable = ({ Orders, OrdersL, deleting, classes, ...props }) => {
  const [DeleteId, setDeleteId] = useState(null);
  const [ModalName, setModalName] = useState(null);
  const [ModalId, setModalId] = useState(null);
  const [open, setOpen] = React.useState(false);

  const [columns] = useState([
    { name: "company", title: "Компания" },
    { name: "saleDate", title: "Дата создания" },
    { name: "status", title: "Статус" },
    { name: " ", title: " " },
  ]);
  const [rows, setRows] = useState([]);
  const [tableColumnExtensions] = useState([{ columnName: " ", width: 110 }]);
  const [selection, setSelection] = useState([]);
  // const [tableStatus, setTableStatus] = useState(false);
  const [theArray, setTheArray] = useState([]);
  let history = useHistory();

  useEffect(() => {
    props.getOrders();
  }, []);
  useEffect(() => {
    if (!deleting) {
      props.getOrders();
    }
  }, [deleting]);
  useEffect(() => {
    if (DeleteId !== null) {
      props.deleteOrders(DeleteId);
    }
  }, [DeleteId]);
  useEffect(() => {
    if (selection !== [] && rows !== []) {
      setTheArray(() => selection.map((st) => rows[st]));
    }
  }, [selection, rows]);
  useEffect(() => {
    if (OrdersL === false) {
      setRows(
        Orders.map((r) => ({
          company: r.company.name,
          saleDate: r.created_at,
          status: r.status !== null ? r.status.name : "",
          " ": (
            <div>
              <IconButton
                style={{ marginRight: 2 }}
                onClick={() => history.push(`/OrdersEdit/${r.id}`)}
              >
                <img
                  src="https://image.flaticon.com/icons/svg/61/61456.svg"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>{" "}
              <IconButton onClick={() => handleClickOpen(r.name, r.id)}>
                <img
                  src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_delete_48px-512.png"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>
            </div>
          ),
        }))
      );
    }
  }, [OrdersL]);

  const handleClickOpen = (name, id) => {
    setModalId(id);
    setModalName(name);
    setOpen(true);
  };

  const handleDelete = (id) => {
    setDeleteId(id);
    setOpen(false);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <DeleteDialog
        ModalName={ModalName}
        ModalId={ModalId}
        ModalOpen={open}
        handleDelete={handleDelete}
        handleClose={handleClose}
      />

      <Paper style={{ maxWidth: 1440, maxHeight: 480, height: "100%" }}>
        {OrdersL === false ? (
          <Grid rows={rows} columns={columns}>
            <SortingState
              defaultSorting={[{ columnName: "company", direction: "asc" }]}
            />
            <IntegratedSorting />
            <SelectionState
              selection={selection}
              onSelectionChange={setSelection}
            />
            <IntegratedSelection />
            <PagingState defaultCurrentPage={0} pageSize={25} />
            <IntegratedPaging />
            <Table
              columnExtensions={tableColumnExtensions}
              tableComponent={TableComponent}
              cellComponent={Cell}
            />
            <TableSelection showSelectAll />
            <TableHeaderRow showSortingControls />
            <PagingPanel />
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Paper>
    </div>
  );
};
const mapStateToProps = (state) => ({
  Orders: state.getOrdersR.Orders,
  OrdersL: state.getOrdersR.OrdersL,
  deleting: state.deleteOrdersR.deleting,
});

export default R.compose(
  connect(mapStateToProps, {
    getOrders,
    deleteOrders,
    saveRows,
  }),
  withStyles(styles)
)(OrdersTable);
