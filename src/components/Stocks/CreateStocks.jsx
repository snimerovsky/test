import React, { useEffect, useState } from "react";
import { Button, Grid, LinearProgress, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import * as R from "ramda";
import { Field, Form, Formik } from "formik";
import Collapse from "@material-ui/core/Collapse/Collapse";
import Alert from "@material-ui/lab/Alert/Alert";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import { useHistory } from "react-router";
import { resetDefault } from "../../redux/actions/resetDefault";
import { Select, TextField } from "formik-material-ui";
import { createStocks } from "../../redux/actions/Stocks/createStocks";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import { getStoreHouses } from "../../redux/actions/StoreHouses/getStoreHouses";
import { getProductTypes } from "../../redux/actions/ProductTypes/getProductTypes";

const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    maxWidth: 610,
    paddingTop: 8,
  },
  DocsContainer: {
    display: "flex",
    flexDirection: "column",
    borderRadius: 2,
    minHeight: 22,
    border: "1px solid rgba(0, 0, 0, 0.12)",
    width: "100%",
    marginTop: 16,
    position: "relative",
  },
  ImgContainer: {
    maxHeight: 440,
  },
  LabelAbsolute: {
    position: "absolute",
    top: -16,
    left: 16,
    padding: 4,
    backgroundColor: "#fafafa",
  },
});

const CreateStocks = ({
  classes,
  creating,
  ProductTypes,
  ProductTypesL,
  StoreHouses,
  StoreHousesL,
  ...props
}) => {
  const [formValues, setFormValues] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [ResetAlert, setResetAlert] = React.useState(false);
  const history = useHistory();

  useEffect(() => {
    props.getStoreHouses();
    props.getProductTypes();
  }, []);

  useEffect(() => {
    if (creating === false) {
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
        setResetAlert(true);
      }, 750);
    }
    return clearTimeout();
  }, [creating]);

  useEffect(() => {
    if (formValues !== null) {
      props.createStocks({
        number: formValues.number,
        product_type: `${formValues.product_type}`,
        storehouse: `${formValues.storehouse}`,
      });
    }
  }, [formValues]);
  useEffect(() => {
    if (ResetAlert === true) {
      props.resetDefault();
      history.push("/stocks");
    }
  }, [ResetAlert]);
  return (
    <div className={classes.Container}>
      {!StoreHousesL && !ProductTypesL ? (
        <>
          <Typography variant="h4"> Создание</Typography>

          <Grid
            container
            justify="flex-start"
            alignItems="center"
            direction="row"
            spacing={2}
            style={{ paddingLeft: 8, marginTop: 16 }}
          >
            <Formik
              initialValues={{
                product_type: "",
                storehouse: "",
                number: "",
              }}
              // validate={(values) => {
              //   const errors = {};
              //   if (!values.email) {
              //     errors.email = "Обязательное поле";
              //   } else if (
              //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
              //       values.email
              //     )
              //   ) {
              //     errors.email = "Invalid email address";
              //   }
              //   return errors;
              // }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  setSubmitting(false);
                  setFormValues(values);
                }, 500);
              }}
            >
              {({ submitForm, isSubmitting }) => (
                <Form>
                  <Grid
                    container
                    justify="flex-end"
                    direction="column"
                    style={{ marginTop: "16px", marginLeft: 8, maxWidth: 400 }}
                  >
                    <Grid container direction="column">
                      <Grid item sm={12}>
                        <Field
                          name="number"
                          label="Номер"
                          type="text"
                          fullWidth
                          component={TextField}
                        />
                      </Grid>
                      <Grid item sm={12}>
                        <FormControl style={{ width: 400 }}>
                          <InputLabel htmlFor="age-simple">
                            Тип аппарата
                          </InputLabel>
                          <Field
                            style={{ width: "100%" }}
                            component={Select}
                            name="product_type"
                            inputProps={{
                              id: "age-simple",
                            }}
                            fullWidth
                          >
                            {ProductTypes.map((pt) => (
                              <MenuItem value={pt.id} key={pt.id}>
                                {pt.name}
                              </MenuItem>
                            ))}
                          </Field>
                        </FormControl>
                      </Grid>
                      <Grid item sm={12}>
                        <FormControl style={{ width: 400 }}>
                          <InputLabel htmlFor="storehouse-simple">
                            Склад
                          </InputLabel>
                          <Field
                            style={{ width: "100%" }}
                            component={Select}
                            name="storehouse"
                            inputProps={{
                              id: "storehouse-simple",
                            }}
                            fullWidth
                          >
                            {StoreHouses.map((st) => (
                              <MenuItem value={st.id} key={st.id}>
                                {st.name}
                              </MenuItem>
                            ))}
                          </Field>
                        </FormControl>
                      </Grid>
                    </Grid>
                    {isSubmitting && <LinearProgress />}

                    <Collapse
                      in={open}
                      style={{ marginTop: "16px", maxWidth: "100%" }}
                    >
                      <Alert
                        severity="success"
                        action={
                          <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                              setOpen(false);
                            }}
                          >
                            <CloseIcon fontSize="inherit" color="primary" />
                          </IconButton>
                        }
                      >
                        Данные изменены
                      </Alert>
                    </Collapse>
                    <div style={{ display: "flex", alignSelf: "flex-end" }}>
                      <Button
                        variant="contained"
                        color="primary"
                        style={{ textTransform: "none" }}
                        disabled={isSubmitting}
                        onClick={submitForm}
                      >
                        Сохранить
                      </Button>
                    </div>
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </>
      ) : null}
    </div>
  );
};

const mapStateToProps = (state) => ({
  creating: state.createStocksR.creating,
  StoreHouses: state.getStoreHousesR.StoreHouses,
  StoreHousesL: state.getStoreHousesR.StoreHousesL,
  ProductTypes: state.getProductTypesR.ProductTypes,
  ProductTypesL: state.getProductTypesR.ProductTypesL,
});

export default R.compose(
  connect(mapStateToProps, {
    getStoreHouses,
    getProductTypes,
    createStocks,
    resetDefault,
  }),
  withStyles(styles)
)(CreateStocks);
