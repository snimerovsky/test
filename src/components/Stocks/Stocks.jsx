import {
  Grid,
  PagingPanel,
  Table,
  TableHeaderRow,
  TableSelection,
} from "@devexpress/dx-react-grid-material-ui";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";

import {
  IntegratedPaging,
  IntegratedSelection,
  IntegratedSorting,
  PagingState,
  SelectionState,
  SortingState,
} from "@devexpress/dx-react-grid";
import * as R from "ramda";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router";
import IconButton from "@material-ui/core/IconButton";
import DeleteDialog from "../DeleteDialog/DeleteDialog";
import {
  Cell,
  TableComponent,
} from "../CustomTableComponents/CustomTableComponents";
import { getStocks } from "../../redux/actions/Stocks/getStocks";
import { deleteStocks } from "../../redux/actions/Stocks/deleteStocks";
import {saveRows} from "../../redux/actions/saveRows";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "row",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "space-between",
    alignItems: "center",
  },
  ButtonGroupFooter: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "flex-end",
    marginTop: 16,
  },
  input: {
    display: "none",
  },
});

const Stocks = ({ Stocks, StocksL, deleting, classes, ...props }) => {
  const [DeleteId, setDeleteId] = useState(null);
  const [ModalName, setModalName] = useState(null);
  const [ModalId, setModalId] = useState(null);
  const [open, setOpen] = useState(false);
  const [columns] = useState([
    { name: "storehouse", title: "Склад" },
    { name: "product_type", title: "Тип аппарата" },
    { name: "number", title: "Номер" },
    { name: " ", title: " " },
  ]);
  const [rows, setRows] = useState([]);
  const [tableColumnExtensions] = useState([{ columnName: " ", width: 110 }]);
  const [selection, setSelection] = useState([]);
  const [tableStatus, setTableStatus] = useState(false);
  const [theArray, setTheArray] = useState([]);
  let history = useHistory();

  useEffect(() => {
    props.getStocks();
  }, []);
  useEffect(() => {
    if (!deleting) {
      props.getStocks();
    }
  }, [deleting]);
  useEffect(() => {
    if (DeleteId !== null) {
      props.deleteStocks(DeleteId);
    }
  }, [DeleteId]);
  useEffect(() => {
    if (selection !== [] && rows !== []) {
      setTheArray(() => selection.map((st) => rows[st]));
    }
  }, [selection, rows]);
  useEffect(() => {
    if (StocksL === false) {
      setRows(
        Stocks.map((r) => ({
          storehouse: r.storehouse.name,
          product_type: r.product_type.name,
          number: r.number,
          " ": (
            <div>
              <IconButton
                style={{ marginRight: 2 }}
                onClick={() => history.push(`/StocksEdit/${r.id}`)}
              >
                <img
                  src="https://image.flaticon.com/icons/svg/61/61456.svg"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>{" "}
              <IconButton onClick={() => handleClickOpen(r.name, r.id)}>
                <img
                  src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_delete_48px-512.png"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>
            </div>
          ),
        }))
      );
    }
  }, [StocksL]);
  const handleClickOpen = (name, id) => {
    setModalId(id);
    setModalName(name);
    setOpen(true);
  };

  const handleDelete = (id) => {
    setDeleteId(id);
    setOpen(false);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleDrop = (event) => {
    console.log("event.target.file[0]", event.target.files);
    if (event.target.files !== undefined) {
      setTableStatus(true);
    }
  };
  const handleRedirect = () => {
    history.push("/detailPage");
    props.saveRows(theArray);
  };

  return (
    <div>
      {console.log("theArray", theArray)}
      <DeleteDialog
        ModalName={ModalName}
        ModalId={ModalId}
        ModalOpen={open}
        handleDelete={handleDelete}
        handleClose={handleClose}
      />
      <div className={classes.ButtonGroup}>
        <Typography variant="h4" style={{ marginBottom: 16, marginTop: 16 }}>
          Остатки
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/createStocks")}
        >
          Добавить
        </Button>
      </div>

      <Paper style={{ maxWidth: 1440, maxHeight: 480, height: "100%" }}>
        {StocksL === false ? (
          // rows={tableStatus === true ? rows : []} columns={columns}
          <Grid rows={rows} columns={columns}>
            <SortingState
              defaultSorting={[{ columnName: "name", direction: "asc" }]}
            />
            <IntegratedSorting />
            <SelectionState
              selection={selection}
              onSelectionChange={setSelection}
            />
            <IntegratedSelection />
            <PagingState defaultCurrentPage={0} pageSize={25} />
            <IntegratedPaging />
            <Table
              columnExtensions={tableColumnExtensions}
              tableComponent={TableComponent}
              cellComponent={Cell}
            />
            <TableSelection showSelectAll />
            <TableHeaderRow showSortingControls />
            <PagingPanel />
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Paper>
      <div className={classes.ButtonGroupFooter}>
        <Button
          variant="contained"
          color="primary"
          onClick={handleRedirect}
          style={{ marginRight: 16 }}
        >
          Создать заявку
        </Button>
        <input
          accept="image/*"
          className={classes.input}
          id="contained-button-file"
          multiple
          type="file"
          onChange={handleDrop}
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" color="primary" component="span">
            Загрузить из файла
          </Button>
        </label>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  Stocks: state.getStocksR.Stocks,
  StocksL: state.getStocksR.StocksL,
  deleting: state.deleteStocksR.deleting,
});

export default R.compose(
  connect(mapStateToProps, {
    getStocks,
    deleteStocks,
    saveRows,
  }),
  withStyles(styles)
)(Stocks);
