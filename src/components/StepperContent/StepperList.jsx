import React, { useState } from "react";
import Link from "@material-ui/core/Link";
import DescriptionIcon from "@material-ui/icons/Description";
import CheckIcon from "@material-ui/icons/Check";
import Button from "@material-ui/core/Button";
const StepperList = () => {
  const [FirstId, setFirstId] = useState(false);
  const [Second, setSecondId] = useState(false);

  return (
    <div
      style={{
        width: "100%",
        maxWidth: 720,
        display: "flex",
        flexDirection: "column"
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
          maxWidth: 460,
          margin: 16,
          marginLeft: 0,
          height: 36,
          alignItems: "center"
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <DescriptionIcon style={{ marginRight: 8 }} />
          <Link>Заявка на постгарантийный ремонт.docx</Link>
        </div>

        {FirstId === true ? (
          <div
            style={{ display: "flex", alignItems: "center", cursor: "pointer" }}
            onClick={() => {
              setFirstId(false);
            }}
          >
            <CheckIcon style={{ marginRight: 8 }} /> Подписан
          </div>
        ) : (
          <Button
              size="small"
            variant="contained"
            color="primary"
            onClick={() => {
              setFirstId(true);
            }}
          >
            Подписать
          </Button>
        )}
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
          maxWidth: 460,
          margin: 16,
          marginLeft: 0,
          height: 36,
          alignItems: "center"
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <DescriptionIcon style={{ marginRight: 8 }} />
          <Link>Заявка на постгарантийный ремонт.docx</Link>
        </div>
        {Second === true ? (
          <div
            style={{ display: "flex", alignItems: "center", cursor: "pointer" }}
            onClick={() => {
              setSecondId(false);
            }}
          >
            <CheckIcon style={{ marginRight: 8 }} /> Подписан
          </div>
        ) : (
          <Button
              size="small"
            variant="contained"
            color="primary"
            onClick={() => {
              setSecondId(true);
            }}
          >
            Подписать
          </Button>
        )}
      </div>

      <Button
        style={{ display: "flex", alignSelf: "flex-end" }}
        size="small"
        variant="contained"
        color="primary"
        onClick={() => {
          setFirstId(true);
          setSecondId(true);
        }}
      >
        Подписать все
      </Button>
    </div>
  );
};

export default StepperList;
