import {
  Grid,
  Table,
  TableHeaderRow,
  TableSelection
} from "@devexpress/dx-react-grid-material-ui";
import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import {
  generateRows,
  globalSalesValues
} from "../../assets/demo-data/generator";
import { IntegratedSelection, SelectionState } from "@devexpress/dx-react-grid";

const RepairTable = ({ tableStatus }) => {
  const [columns] = useState([
    { name: "region", title: "Склад" },
    { name: "sector", title: "Тип аппарата" },
    { name: "amount", title: "Номер" }
  ]);
  const [rows] = useState(
    generateRows({ columnValues: globalSalesValues, length: 8 })
  );
  const [selection, setSelection] = useState([]);

  return (
    <Paper style={{ maxWidth: 1440, maxHeight: 480, height: '100%'}}>
      <Grid rows={tableStatus === true ? rows : []} columns={columns}>
        <SelectionState
          selection={selection}
          onSelectionChange={setSelection}
        />
        <IntegratedSelection />
        <Table height="auto"/>
        <TableHeaderRow />
        <TableSelection showSelectAll />
      </Grid>
    </Paper>
  );
};

export default RepairTable;
