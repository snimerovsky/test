import {
  Grid,
  PagingPanel,
  Table,
  TableHeaderRow,
} from "@devexpress/dx-react-grid-material-ui";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";

import {
  IntegratedPaging,
  IntegratedSorting,
  PagingState,
  SortingState,
} from "@devexpress/dx-react-grid";
import * as R from "ramda";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import { getStoreHouses } from "../../redux/actions/StoreHouses/getStoreHouses";
import { useHistory } from "react-router";
import IconButton from "@material-ui/core/IconButton";
import { deleteStoreHouses } from "../../redux/actions/StoreHouses/deleteStoreHouses";
import Button from "@material-ui/core/Button";
import DeleteDialog from "../DeleteDialog/DeleteDialog";
import {Cell, TableComponent} from "../CustomTableComponents/CustomTableComponents";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "space-between",
    alignItems: "center",
  },
});

const StoreHouses = ({
  StoreHouses,
  StoreHousesL,
  deleting,
  classes,
  ...props
}) => {
  const [DeleteId, setDeleteId] = useState(null);
  const [ModalName, setModalName] = useState(null);
  const [ModalId, setModalId] = useState(null);
  const [open, setOpen] = React.useState(false);

  const [columns] = useState([
    { name: "name", title: "Название" },
    { name: "address", title: "Адрес" },
    { name: " ", title: " " },
  ]);
  const [rows, setRows] = useState([]);
  const [tableColumnExtensions] = useState([{ columnName: " ", width: 120 }]);
  let history = useHistory();

  useEffect(() => {
    props.getStoreHouses();
  }, []);
  useEffect(() => {
    if (!deleting) {
      props.getStoreHouses();
    }
  }, [deleting]);
  useEffect(() => {
    if (DeleteId !== null) {
      props.deleteStoreHouses(DeleteId);
    }
  }, [DeleteId]);

  const handleClickOpen = (name, id) => {
    setModalId(id);
    setModalName(name);
    setOpen(true);
  };

  const handleDelete = (id) => {
    setDeleteId(id);
    setOpen(false);
  };
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (StoreHousesL === false) {
      setRows(
        StoreHouses.map((r) => ({
          name: r.name,
          address: r.address,
          " ": (
            <div>
              <IconButton
                style={{ marginRight: 2 }}
                onClick={() => history.push(`/storehousesedit/${r.id}`)}
              >
                <img
                  src="https://image.flaticon.com/icons/svg/61/61456.svg"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>{" "}
              <IconButton onClick={() => handleClickOpen(r.name, r.id)}>
                <img
                  src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_delete_48px-512.png"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>
            </div>
          ),
        }))
      );
    }
  }, [StoreHousesL]);

  return (
    <div>
      <DeleteDialog
        ModalName={ModalName}
        ModalId={ModalId}
        ModalOpen={open}
        handleDelete={handleDelete}
        handleClose={handleClose}
      />
      <div className={classes.ButtonGroup}>
        <Typography variant="h4" style={{ marginBottom: 16, marginTop: 16 }}>
          Склад
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/createStoreHouses")}
        >
          Добавить
        </Button>
      </div>
      <Paper style={{ maxWidth: 1440, maxHeight: 480, height: "100%" }}>
        {StoreHousesL === false ? (
          <Grid rows={rows} columns={columns}>
            <SortingState
              defaultSorting={[{ columnName: "name", direction: "asc" }]}
            />
            <IntegratedSorting />
            <PagingState defaultCurrentPage={0} pageSize={25} />
            <IntegratedPaging />
            <Table
              columnExtensions={tableColumnExtensions}
              tableComponent={TableComponent}
              cellComponent={Cell}
            />
            <TableHeaderRow showSortingControls />
            <PagingPanel />
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Paper>
    </div>
  );
};
const mapStateToProps = (state) => ({
  StoreHouses: state.getStoreHousesR.StoreHouses,
  StoreHousesL: state.getStoreHousesR.StoreHousesL,
  deleting: state.deleteStoreHousesR.deleting,
});

export default R.compose(
  connect(mapStateToProps, {
    getStoreHouses,
    deleteStoreHouses,
  }),
  withStyles(styles)
)(StoreHouses);
