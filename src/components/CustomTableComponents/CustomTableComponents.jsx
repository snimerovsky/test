import { Table } from "@devexpress/dx-react-grid-material-ui";
import { withStyles } from "@material-ui/core";
import React from "react";

const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
});

const WordWrapCell = ({ value, style, ...restProps }) => (
  <Table.Cell
    {...restProps}
    style={{
      ...style,
      height: "auto",
    }}
  >
    <span
      style={{
        width: 190,
      }}
    >
      {value}
    </span>
  </Table.Cell>
);
export const Cell = (props) => {
  const { column } = props;
  if (column.name === "available_truck_categories") {
    return <WordWrapCell {...props} />;
  }
  return <Table.Cell {...props} />;
};

const TableComponentBase = ({ classes, ...restProps }) => (
  <Table.Table {...restProps} className={classes.tableStriped} />
);
export const TableComponent = withStyles(styles, { name: "TableComponent" })(
  TableComponentBase
);

