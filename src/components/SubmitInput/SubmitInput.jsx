import React from "react";
import MuiTextField from "@material-ui/core/TextField/TextField";
import { fieldToTextField } from "formik-material-ui";

export const SubmitInput = (props) => {
  const {
    form: { setFieldValue, submitForm },
    field: { name },
  } = props;
  const onKeyPress = React.useCallback(
    (event) => {
      if (event.key === "Enter") {
        const { value } = event.target;
        submitForm();
      }
    },
    [setFieldValue, name]
  );
  return <MuiTextField {...fieldToTextField(props)} onKeyPress={onKeyPress} />;
};
