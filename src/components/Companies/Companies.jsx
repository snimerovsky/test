import {
  Grid,
  PagingPanel,
  Table,
  TableHeaderRow,
} from "@devexpress/dx-react-grid-material-ui";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";

import {
  IntegratedPaging,
  IntegratedSorting,
  PagingState,
  SortingState,
} from "@devexpress/dx-react-grid";
import * as R from "ramda";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import { getCompanies } from "../../redux/actions/Company/getCompanies";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import {
  Cell,
  TableComponent,
} from "../CustomTableComponents/CustomTableComponents";
import IconButton from "@material-ui/core/IconButton";
import DeleteDialog from "../DeleteDialog/DeleteDialog";
import {deleteProductTypes} from "../../redux/actions/ProductTypes/deleteProductTypes";
import {deleteCompanies} from "../../redux/actions/Company/deleteCompanies";
const styles = (theme) => ({
  margin: {
    margin: theme.spacing.unit * 1,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  FormC: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  Container: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2,
    paddingTop: 8,
  },
  ImgContainer: {
    maxHeight: 440,
  },
  ButtonGroup: {
    display: "flex",
    width: "100%",
    maxWidth: 1440,
    justifyContent: "space-between",
    alignItems: "center",
  },
});

const Companies = ({ companies, companiesL, deleting, classes, ...props }) => {
  const [DeleteId, setDeleteId] = useState(null);
  const [ModalName, setModalName] = useState(null);
  const [ModalId, setModalId] = useState(null);
  const [open, setOpen] = React.useState(false);
  const [tableColumnExtensions] = useState([{ columnName: " ", width: 110 }]);
  let history = useHistory();


  const [columns] = useState([
    { name: "name", title: "Название" },
    { name: "inn", title: "ИНН" },
    { name: "kpp", title: "КПП" },
    { name: "address", title: "Адрес" },
    { name: " ", title: " " },
  ]);
  const [rows, setRows] = useState([]);
  useEffect(() => {
    props.getCompanies();
  }, []);

  useEffect(() => {
    if (!deleting) {
      props.getCompanies();
    }
  }, [deleting]);
  useEffect(() => {
    if (DeleteId !== null) {
      props.deleteCompanies(DeleteId);
    }
  }, [DeleteId]);
  useEffect(() => {
    if (companiesL === false) {
      setRows(
        companies.map((r) => ({
          name: r.name,
          inn: r.inn,
          kpp: r.kpp,
          address: r.address,

          " ": (
            <div>
              <IconButton
                style={{ marginRight: 2 }}
                onClick={() => history.push(`/companiesedit/${r.id}`)}
              >
                <img
                  src="https://image.flaticon.com/icons/svg/61/61456.svg"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>{" "}
              <IconButton onClick={() => handleClickOpen(r.name, r.id)}>
                <img
                  src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_delete_48px-512.png"
                  alt=""
                  style={{ width: 20, height: 20 }}
                />
              </IconButton>
            </div>
          ),
        }))
      );
    }
  }, [companiesL]);
  const handleClickOpen = (name, id) => {
    setModalId(id);
    setModalName(name);
    setOpen(true);
  };

  const handleDelete = (id) => {
    setDeleteId(id);
    setOpen(false);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <DeleteDialog
        ModalName={ModalName}
        ModalId={ModalId}
        ModalOpen={open}
        handleDelete={handleDelete}
        handleClose={handleClose}
      />
      <div className={classes.ButtonGroup}>
        <Typography variant="h4" style={{ marginBottom: 16, marginTop: 16 }}>
          Организации
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/createCompanies")}
        >
          Добавить
        </Button>
      </div>

      <Paper style={{ maxWidth: 1440, maxHeight: 480, height: "100%" }}>
        {companiesL === false ? (
          <Grid rows={rows} columns={columns}>
            <SortingState
              defaultSorting={[{ columnName: "name", direction: "asc" }]}
            />
            <IntegratedSorting />
            <PagingState defaultCurrentPage={0} pageSize={25} />
            <IntegratedPaging />
            <Table
              columnExtensions={tableColumnExtensions}
              tableComponent={TableComponent}
              cellComponent={Cell}
            />
            <TableHeaderRow showSortingControls />
            <PagingPanel />
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Paper>
    </div>
  );
};
const mapStateToProps = (state) => ({
  companies: state.getCompaniesR.companies,
  companiesL: state.getCompaniesR.companiesL,
  deleting: state.deleteCompaniesR.deleting,
});

export default R.compose(
  connect(mapStateToProps, {
    getCompanies,
    deleteCompanies,
  }),
  withStyles(styles)
)(Companies);
