import React, { useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { NavLink } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useHistory } from "react-router";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import * as R from "ramda";
import { connect } from "react-redux";
import { getProfile } from "../../redux/actions/Profile/getProfile";
const MenuData = [
  { path: "/tickets", name: "Заявки" },
  { path: "/stocks", name: "В ремонт" },
];
const AdminSettings = [
  { path: "/companies", name: " Организации" },
  { path: "/users", name: " Пользователи" },
  { path: "/storehouses", name: " Склады" },
  { path: "/product-types", name: " Типы аппаратов" },
];
const EmployeeSettings = [
  { path: "/profile", name: "Профиль" },
  { path: "/company", name: " Организация" },
];
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  toolbar: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));
const SidebarMenu = ({ profile, profileL, ...props }) => {
  const [open, setOpen] = React.useState(true);
  useEffect(() => {
    props.getProfile();
  }, []);
  const classes = useStyles();
  const history = useHistory();
  const handleClick = () => {
    setOpen(!open);
  };
  return (
    <div>
      {profileL === false ? (
        <div>
          <div className={classes.toolbar} />
          <List component="nav" aria-labelledby="nested-list-subheader">
            {MenuData.map((item, index) => (
              <ListItem
                button
                exact
                key={item.name}
                component={NavLink}
                to={item.path}
                activeClassName="Mui-selected"
                style={{
                  color: "#000",
                  textDecoration: "none",
                  underline: "none",
                }}
              >
                <ListItemIcon>
                  {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                {item.name}
              </ListItem>
            ))}
            <ListItem button onClick={handleClick}>
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Настройки" />
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {profile.role.name !== "Employees"
                  ? EmployeeSettings.map((item, index) => (
                      <ListItem
                        button
                        exact
                        className={classes.nested}
                        key={item.name}
                        component={NavLink}
                        to={item.path}
                        activeClassName="Mui-selected"
                      >
                        <ListItemIcon>
                          {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                        </ListItemIcon>
                        {item.name}
                      </ListItem>
                    ))
                  : AdminSettings.map((item, index) => (
                      <ListItem
                        button
                        exact
                        className={classes.nested}
                        key={item.name}
                        component={NavLink}
                        to={item.path}
                        activeClassName="Mui-selected"
                      >
                        <ListItemIcon>
                          {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                        </ListItemIcon>
                        {item.name}
                      </ListItem>
                    ))}
              </List>
            </Collapse>
            <ListItem
              button
              exact
              key={123123123213}
              component={NavLink}
              to="/"
              activeClassName="Mui-selected"
              style={{
                color: "#000",
                textDecoration: "none",
                underline: "none",
              }}
              onClick={() => {
                localStorage.clear();
                history.push("/");
              }}
            >
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              Выход
            </ListItem>
          </List>
        </div>
      ) : null}
    </div>
  );
};
const mapStateToProps = (state) => ({
  profile: state.getProfileR.profile,
  profileL: state.getProfileR.profileL,
});

export default R.compose(connect(mapStateToProps, { getProfile }))(SidebarMenu);
