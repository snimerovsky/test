import {
  Grid,
  Table,
  TableEditColumn,
  TableEditRow,
  TableHeaderRow,
} from "@devexpress/dx-react-grid-material-ui";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import { EditingState } from "@devexpress/dx-react-grid";

const RepairTable = ({ Rows, matchPage, changeTableData }) => {
  const [columns] = useState([
    { name: "storehouse", title: "Склад" },
    { name: "product_type", title: "Тип" },
    { name: "number", title: "Номер" },
  ]);
  const [rows, setRows] = useState([]);
  const [editingRowIds, setEditingRowIds] = useState([]);
  const [addedRows, setAddedRows] = useState([]);
  const [rowChanges, setRowChanges] = useState({});
  useEffect(() => {
    if (Rows !== [] && matchPage === "stocks") {
      setRows(
        Rows.SavedRows.map((r) => ({
          storehouse: r.storehouse,
          product_type: r.product_type,
          number: r.number,
        }))
      );
    }
    if (matchPage === "ticket" && Rows !== []) {
      setRows(
        Rows.map((r) => ({
          storehouse: r.storehouse !== null ? r.storehouse.name : "",
          product_type: r.product_type !== null ? r.product_type.name : "",
          number: r.number || "",
        }))
      );
    } else {
      setRows([]);
    }
  }, []);
  const getRowId = (row) => row.id;
  const changeAddedRows = (value) => {
    const initialized = value.map((row) =>
      Object.keys(row).length ? row : ""
    );
    setAddedRows(initialized);
  };

  const commitChanges = ({ added, changed, deleted }) => {
    let changedRows;
    if (added) {
      const startingAddedId =
        rows.length > 0 ? rows[rows.length - 1].id + 1 : 0;
      changedRows = [
        ...rows,
        ...added.map((row, index) => ({
          id: startingAddedId + index,
          ...row,
        })),
      ];
    }
    if (changed) {
      changedRows = rows.map((row) =>
        changed[row.id] ? { ...row, ...changed[row.id] } : row
      );
    }
    if (deleted) {
      const deletedSet = new Set(deleted);
      changedRows = rows.filter((row) => !deletedSet.has(row.id));
    }
    changeTableData(changedRows);
    setRows(changedRows);
  };
  return (
    <Paper style={{ maxWidth: 1440, maxHeight: 480, height: "100%" }}>
      <Grid rows={rows} columns={columns} getRowId={getRowId}>
        <EditingState
          editingRowIds={editingRowIds}
          onEditingRowIdsChange={setEditingRowIds}
          rowChanges={rowChanges}
          onRowChangesChange={setRowChanges}
          addedRows={addedRows}
          onAddedRowsChange={changeAddedRows}
          onCommitChanges={commitChanges}
        />
        <Table height="auto" />
        <TableHeaderRow />
        <TableEditRow />
        <TableEditColumn
          showAddCommand={!addedRows.length}
          showEditCommand
          showDeleteCommand
        />
      </Grid>
    </Paper>
  );
};

export default RepairTable;
