import axios from "axios";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";

export const login = (identifier, password) => {
  return (dispatch) => {
    dispatch({
      type: LOGIN_REQUEST,
      payload: {},
    });
    axios
      .post(`https://api-dev.asso-rail.ru//auth/local`, { identifier, password })
      .then((response) => {
        const data = response.data;
        dispatch({
          type: LOGIN_SUCCESS,
          payload: data,
        });
      })
      .catch((data) => {
        dispatch({
          type: LOGIN_ERROR,
          payload:
            data.message === 'Request failed with status code 400'
              ? "Неверные данные: логин/пароль"
              : data.message,
        });
      });
  };
};
