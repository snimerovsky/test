import axios from "axios";
import { baseURL } from "../../utils/axiosConfig";

export const RESET_PASSWORD_REQUEST = "RESET_PASSWORD_REQUEST";
export const RESET_PASSWORD_SUCCESS = "RESET_PASSWORD_SUCCESS";
export const RESET_PASSWORD_ERROR = "RESET_PASSWORD_ERROR";

export const resetPassword = (email) => {
  return (dispatch) => {
    dispatch({
      type: RESET_PASSWORD_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/auth/forgot-password`, {
        email: email,
        url: `${baseURL}/admin/plugins/users-permissions/auth/reset-password`,
      })
      .then((response) => {
        const data = response.data;
        dispatch({
          type: RESET_PASSWORD_SUCCESS,
          payload: data,
        });
      })
      .catch((data) => {
        dispatch({
          type: RESET_PASSWORD_ERROR,
          payload:
            data.message === "Request failed with status code 400"
              ? "Невалидный email"
              : data.message,
        });
      });
  };
};
