import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_COMPANIES_REQUEST = "DELETE_COMPANIES_REQUEST";
export const DELETE_COMPANIES_SUCCESS = "DELETE_COMPANIES_SUCCESS";
export const DELETE_COMPANIES_ERROR = "DELETE_COMPANIES_ERROR";

export const deleteCompanies = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_COMPANIES_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/companies/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_COMPANIES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_COMPANIES_ERROR,
          payload: err,
        });
      });
  };
};
