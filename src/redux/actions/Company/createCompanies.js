import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_COMPANIES_REQUEST = "CREATE_COMPANIES_REQUEST";
export const CREATE_COMPANIES_SUCCESS = "CREATE_COMPANIES_SUCCESS";
export const CREATE_COMPANIES_ERROR = "CREATE_COMPANIES_ERROR";

export const createCompanies = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_COMPANIES_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/companies`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_COMPANIES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_COMPANIES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
