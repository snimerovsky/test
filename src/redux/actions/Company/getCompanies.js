import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_COMPANIES_REQUEST = "GET_COMPANIES_REQUEST";
export const GET_COMPANIES_SUCCESS = "GET_COMPANIES_SUCCESS";
export const GET_COMPANIES_ERROR = "GET_COMPANIES_ERROR";

export const getCompanies = () => {
  return (dispatch) => {
    dispatch({
      type: GET_COMPANIES_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/companies`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_COMPANIES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_COMPANIES_ERROR,
          payload: err,
        });
      });
  };
};
