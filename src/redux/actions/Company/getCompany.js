import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_COMPANY_REQUEST = "GET_COMPANY_REQUEST";
export const GET_COMPANY_SUCCESS = "GET_COMPANY_SUCCESS";
export const GET_COMPANY_ERROR = "GET_COMPANY_ERROR";

export const getCompany = () => {
  return (dispatch) => {
    dispatch({
      type: GET_COMPANY_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/companies/me`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_COMPANY_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_COMPANY_ERROR,
          payload: err,
        });
      });
  };
};
