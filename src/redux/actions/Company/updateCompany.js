import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";
// import {DISPACH_CREATING} from "./createSourse";

export const UPDATE_COMPANY_REQUEST = "UPDATE_COMPANY_REQUEST";
export const UPDATE_COMPANY_SUCCESS = "UPDATE_COMPANY_SUCCESS";
export const UPDATE_COMPANY_ERROR = "UPDATE_COMPANY_ERROR";
// export const DISPACH_UPDATING = "DISPACH_UPDATING";

export const updateCompany = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_COMPANY_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/companies/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_COMPANY_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_COMPANY_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
