export const SAVE_ROWS = "SAVE_ROWS";
export const saveRows = (theArray) => (dispatch) =>
  dispatch({ type: SAVE_ROWS, payload: theArray });
