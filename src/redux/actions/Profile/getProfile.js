import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_PROFILE_REQUEST = "GET_PROFILE_REQUEST";
export const GET_PROFILE_SUCCESS = "GET_PROFILE_SUCCESS";
export const GET_PROFILE_ERROR = "GET_PROFILE_ERROR";

export const getProfile = () => {
  return (dispatch) => {
    dispatch({
      type: GET_PROFILE_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/users/me`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_PROFILE_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PROFILE_ERROR,
          payload: err,
        });
      });
  };
};
