import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_SOURCE_REQUEST = "CREATE_SOURCE_REQUEST";
export const CREATE_SOURCE_SUCCESS = "CREATE_SOURCE_SUCCESS";
export const CREATE_SOURCE_ERROR = "CREATE_SOURCE_ERROR";
export const DISPACH_CREATING = "DISPACH_CREATING";

export const createSource = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_SOURCE_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/lists/truck-categories`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_SOURCE_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_SOURCE_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
export const changedValueCreating = () => (dispatch) =>
  dispatch({ type: DISPACH_CREATING, payload: dispatch.payload });
