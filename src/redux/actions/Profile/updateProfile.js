import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";
// import {DISPACH_CREATING} from "./createSourse";

export const UPDATE_PROFILE_REQUEST = "UPDATE_PROFILE_REQUEST";
export const UPDATE_PROFILE_SUCCESS = "UPDATE_PROFILE_SUCCESS";
export const UPDATE_PROFILE_ERROR = "UPDATE_PROFILE_ERROR";
// export const DISPACH_UPDATING = "DISPACH_UPDATING";

export const updateProfile = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_PROFILE_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/users/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_PROFILE_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_PROFILE_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
