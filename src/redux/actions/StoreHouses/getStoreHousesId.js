import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_STORE_HOUSES_ID_REQUEST = "GET_STORE_HOUSES_ID_REQUEST";
export const GET_STORE_HOUSES_ID_SUCCESS = "GET_STORE_HOUSES_ID_SUCCESS";
export const GET_STORE_HOUSES_ID_ERROR = "GET_STORE_HOUSES_ID_ERROR";

export const getStoreHousesId = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_STORE_HOUSES_ID_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/storehouses/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_STORE_HOUSES_ID_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_STORE_HOUSES_ID_ERROR,
          payload: err,
        });
      });
  };
};
