import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const UPDATE_STORE_HOUSES_REQUEST = "UPDATE_STORE_HOUSES_REQUEST";
export const UPDATE_STORE_HOUSES_SUCCESS = "UPDATE_STORE_HOUSES_SUCCESS";
export const UPDATE_STORE_HOUSES_ERROR = "UPDATE_STORE_HOUSES_ERROR";

export const updateStoreHouses = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_STORE_HOUSES_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/storehouses/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_STORE_HOUSES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_STORE_HOUSES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
