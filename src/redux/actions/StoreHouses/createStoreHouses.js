import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_STORE_HOUSES_REQUEST = "CREATE_STORE_HOUSES_REQUEST";
export const CREATE_STORE_HOUSES_SUCCESS = "CREATE_STORE_HOUSES_SUCCESS";
export const CREATE_STORE_HOUSES_ERROR = "CREATE_STORE_HOUSES_ERROR";

export const createStoreHouses = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_STORE_HOUSES_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/storehouses`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_STORE_HOUSES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_STORE_HOUSES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
