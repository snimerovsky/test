import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_STORE_HOUSES_REQUEST = "DELETE_STORE_HOUSES_REQUEST";
export const DELETE_STORE_HOUSES_SUCCESS = "DELETE_STORE_HOUSES_SUCCESS";
export const DELETE_STORE_HOUSES_ERROR = "DELETE_STORE_HOUSES_ERROR";

export const deleteStoreHouses = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_STORE_HOUSES_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/storehouses/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_STORE_HOUSES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_STORE_HOUSES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
