import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_STOREHOUSES_REQUEST = "GET_STOREHOUSES_REQUEST";
export const GET_STOREHOUSES_SUCCESS = "GET_STOREHOUSES_SUCCESS";
export const GET_STOREHOUSES_ERROR = "GET_STOREHOUSES_ERROR";

export const getStoreHouses = () => {
  return (dispatch) => {
    dispatch({
      type: GET_STOREHOUSES_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/storehouses`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_STOREHOUSES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_STOREHOUSES_ERROR,
          payload: err,
        });
      });
  };
};
