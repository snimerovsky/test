import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_USERS_REQUEST = "DELETE_USERS_REQUEST";
export const DELETE_USERS_SUCCESS = "DELETE_USERS_SUCCESS";
export const DELETE_USERS_ERROR = "DELETE_USERS_ERROR";

export const deleteUsers = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_USERS_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/users/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_USERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_USERS_ERROR,
          payload: err,
        });
      });
  };
};
