import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_USERS_REQUEST = "GET_USERS_REQUEST";
export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const GET_USERS_ERROR = "GET_USERS_ERROR";

export const getUsers = () => {
  return (dispatch) => {
    dispatch({
      type: GET_USERS_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/users`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_USERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_USERS_ERROR,
          payload: err,
        });
      });
  };
};
