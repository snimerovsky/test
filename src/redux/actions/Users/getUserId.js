import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_USERS_ID_REQUEST = "GET_USERS_ID_REQUEST";
export const GET_USERS_ID_SUCCESS = "GET_USERS_ID_SUCCESS";
export const GET_USERS_ID_ERROR = "GET_USERS_ID_ERROR";

export const getUserId = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_USERS_ID_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/users/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_USERS_ID_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_USERS_ID_ERROR,
          payload: err,
        });
      });
  };
};
