import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_USERS_REQUEST = "CREATE_USERS_REQUEST";
export const CREATE_USERS_SUCCESS = "CREATE_USERS_SUCCESS";
export const CREATE_USERS_ERROR = "CREATE_USERS_ERROR";

export const createUsers = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_USERS_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/users`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_USERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_USERS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
