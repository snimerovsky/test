import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_PRODUCT_TYPES_REQUEST = "DELETE_PRODUCT_TYPES_REQUEST";
export const DELETE_PRODUCT_TYPES_SUCCESS = "DELETE_PRODUCT_TYPES_SUCCESS";
export const DELETE_PRODUCT_TYPES_ERROR = "DELETE_PRODUCT_TYPES_ERROR";

export const deleteProductTypes = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_PRODUCT_TYPES_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/product-types/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_PRODUCT_TYPES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_PRODUCT_TYPES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
