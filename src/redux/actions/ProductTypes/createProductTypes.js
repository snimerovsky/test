import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_PRODUCT_TYPES_REQUEST = "CREATE_PRODUCT_TYPES_REQUEST";
export const CREATE_PRODUCT_TYPES_SUCCESS = "CREATE_PRODUCT_TYPES_SUCCESS";
export const CREATE_PRODUCT_TYPES_ERROR = "CREATE_PRODUCT_TYPES_ERROR";

export const createProductTypes = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_PRODUCT_TYPES_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/product-types/`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_PRODUCT_TYPES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_PRODUCT_TYPES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
