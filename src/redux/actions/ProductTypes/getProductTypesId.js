import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_PRODUCT_TYPES_ID_REQUEST = "GET_PRODUCT_TYPES_ID_REQUEST";
export const GET_PRODUCT_TYPES_ID_SUCCESS = "GET_PRODUCT_TYPES_ID_SUCCESS";
export const GET_PRODUCT_TYPES_ID_ERROR = "GET_PRODUCT_TYPES_ID_ERROR";

export const getProductTypesId = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_PRODUCT_TYPES_ID_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/product-types/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_PRODUCT_TYPES_ID_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PRODUCT_TYPES_ID_ERROR,
          payload: err,
        });
      });
  };
};
