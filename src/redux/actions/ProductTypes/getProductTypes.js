import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_PRODUCT_TYPES_REQUEST = "GET_PRODUCT_TYPES_REQUEST";
export const GET_PRODUCT_TYPES_SUCCESS = "GET_PRODUCT_TYPES_SUCCESS";
export const GET_PRODUCT_TYPES_ERROR = "GET_PRODUCT_TYPES_ERROR";

export const getProductTypes = () => {
  return (dispatch) => {
    dispatch({
      type: GET_PRODUCT_TYPES_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/product-types`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_PRODUCT_TYPES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PRODUCT_TYPES_ERROR,
          payload: err,
        });
      });
  };
};
