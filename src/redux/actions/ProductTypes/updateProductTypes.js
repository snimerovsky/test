import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const UPDATE_PRODUCT_TYPES_REQUEST = "UPDATE_PRODUCT_TYPES_REQUEST";
export const UPDATE_PRODUCT_TYPES_SUCCESS = "UPDATE_PRODUCT_TYPES_SUCCESS";
export const UPDATE_PRODUCT_TYPES_ERROR = "UPDATE_PRODUCT_TYPES_ERROR";

export const updateProductTypes = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_PRODUCT_TYPES_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/product-types/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_PRODUCT_TYPES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_PRODUCT_TYPES_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
