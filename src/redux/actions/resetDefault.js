export const RESET_DEFAULT = "RESET_DEFAULT";
export const resetDefault = () => (dispatch) =>
  dispatch({ type: RESET_DEFAULT, payload: dispatch.payload });
