import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_STOCKS_REQUEST = "GET_STOCKS_REQUEST";
export const GET_STOCKS_SUCCESS = "GET_STOCKS_SUCCESS";
export const GET_STOCKS_ERROR = "GET_STOCKS_ERROR";

export const getStocks = () => {
  return (dispatch) => {
    dispatch({
      type: GET_STOCKS_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/stocks`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_STOCKS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_STOCKS_ERROR,
          payload: err,
        });
      });
  };
};
