import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_STOCKS_REQUEST = "CREATE_STOCKS_REQUEST";
export const CREATE_STOCKS_SUCCESS = "CREATE_STOCKS_SUCCESS";
export const CREATE_STOCKS_ERROR = "CREATE_STOCKS_ERROR";

export const createStocks = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_STOCKS_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/stocks/`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_STOCKS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_STOCKS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
