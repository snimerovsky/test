import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const UPDATE_STOCKS_REQUEST = "UPDATE_STOCKS_REQUEST";
export const UPDATE_STOCKS_SUCCESS = "UPDATE_STOCKS_SUCCESS";
export const UPDATE_STOCKS_ERROR = "UPDATE_STOCKS_ERROR";

export const updateStocks = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_STOCKS_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/stocks/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_STOCKS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_STOCKS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
