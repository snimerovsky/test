import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_STOCKS_REQUEST = "DELETE_STOCKS_REQUEST";
export const DELETE_STOCKS_SUCCESS = "DELETE_STOCKS_SUCCESS";
export const DELETE_STOCKS_ERROR = "DELETE_STOCKS_ERROR";

export const deleteStocks = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_STOCKS_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/stocks/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_STOCKS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_STOCKS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
