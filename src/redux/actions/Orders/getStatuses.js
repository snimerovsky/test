import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_STATUSES_REQUEST = "GET_STATUSES_REQUEST";
export const GET_STATUSES_SUCCESS = "GET_STATUSES_SUCCESS";
export const GET_STATUSES_ERROR = "GET_STATUSES_ERROR";

export const getStatuses = () => {
  return (dispatch) => {
    dispatch({
      type: GET_STATUSES_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/statuses`, axiosConfig)
      .then((response) => {
        const data = response.data;
        console.log("statuses", data);
        dispatch({
          type: GET_STATUSES_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_STATUSES_ERROR,
          payload: err,
        });
      });
  };
};
