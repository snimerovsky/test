import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_DOCS_REQUEST = "GET_DOCS_REQUEST";
export const GET_DOCS_SUCCESS = "GET_DOCS_SUCCESS";
export const GET_DOCS_ERROR = "GET_DOCS_ERROR";

export const getDocs = () => {
    return (dispatch) => {
        dispatch({
            type: GET_DOCS_REQUEST,
            payload: {},
        });
        axios
            .get(`${baseURL}/docs`, axiosConfig)
            .then((response) => {
                const data = response.data;
                dispatch({
                    type: GET_DOCS_SUCCESS,
                    payload: data,
                });
            })
            .catch((err) => {
                dispatch({
                    type: GET_DOCS_ERROR,
                    payload: err,
                });
            });
    };
};
