import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_ORDERS_ID_REQUEST = "GET_ORDERS_ID_REQUEST";
export const GET_ORDERS_ID_SUCCESS = "GET_ORDERS_ID_SUCCESS";
export const GET_ORDERS_ID_ERROR = "GET_ORDERS_ID_ERROR";

export const getOrdersId = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_ORDERS_ID_REQUEST,
      payload: {},
    });
    console.log(id);
    axios
      .get(`${baseURL}/tickets/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_ORDERS_ID_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_ORDERS_ID_ERROR,
          payload: err,
        });
      });
  };
};
