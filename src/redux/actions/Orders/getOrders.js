import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_ORDERS_REQUEST = "GET_ORDERS_REQUEST";
export const GET_ORDERS_SUCCESS = "GET_ORDERS_SUCCESS";
export const GET_ORDERS_ERROR = "GET_ORDERS_ERROR";

export const getOrders = () => {
  return (dispatch) => {
    dispatch({
      type: GET_ORDERS_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/tickets`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_ORDERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_ORDERS_ERROR,
          payload: err,
        });
      });
  };
};
