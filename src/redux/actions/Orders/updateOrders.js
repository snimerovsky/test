import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const UPDATE_ORDERS_REQUEST = "UPDATE_ORDERS_REQUEST";
export const UPDATE_ORDERS_SUCCESS = "UPDATE_ORDERS_SUCCESS";
export const UPDATE_ORDERS_ERROR = "UPDATE_ORDERS_ERROR";

export const updateOrders = (id, sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_ORDERS_REQUEST,
      payload: {},
    });
    axios
      .put(`${baseURL}/tickets/${id}`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: UPDATE_ORDERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: UPDATE_ORDERS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
