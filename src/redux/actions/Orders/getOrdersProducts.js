import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const GET_ORDERS_PRODUCTS_REQUEST = "GET_ORDERS_PRODUCTS_REQUEST";
export const GET_ORDERS_PRODUCTS_SUCCESS = "GET_ORDERS_PRODUCTS_SUCCESS";
export const GET_ORDERS_PRODUCTS_ERROR = "GET_ORDERS_PRODUCTS_ERROR";

export const getOrdersProducts = () => {
  return (dispatch) => {
    dispatch({
      type: GET_ORDERS_PRODUCTS_REQUEST,
      payload: {},
    });
    axios
      .get(`${baseURL}/ticket-products`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: GET_ORDERS_PRODUCTS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_ORDERS_PRODUCTS_ERROR,
          payload: err,
        });
      });
  };
};
