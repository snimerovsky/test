import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const DELETE_ORDERS_REQUEST = "DELETE_ORDERS_REQUEST";
export const DELETE_ORDERS_SUCCESS = "DELETE_ORDERS_SUCCESS";
export const DELETE_ORDERS_ERROR = "DELETE_ORDERS_ERROR";

export const deleteOrders = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_ORDERS_REQUEST,
      payload: {},
    });
    axios
      .delete(`${baseURL}/tickets/${id}`, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: DELETE_ORDERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: DELETE_ORDERS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
