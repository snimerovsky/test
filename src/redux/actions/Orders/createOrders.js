import axios from "axios";
import { axiosConfig, baseURL } from "../../../utils/axiosConfig";

export const CREATE_ORDERS_REQUEST = "CREATE_ORDERS_REQUEST";
export const CREATE_ORDERS_SUCCESS = "CREATE_ORDERS_SUCCESS";
export const CREATE_ORDERS_ERROR = "CREATE_ORDERS_ERROR";

export const createOrders = (sourceBody) => {
  return (dispatch) => {
    dispatch({
      type: CREATE_ORDERS_REQUEST,
      payload: {},
    });
    axios
      .post(`${baseURL}/tickets/`, sourceBody, axiosConfig)
      .then((response) => {
        const data = response.data;
        dispatch({
          type: CREATE_ORDERS_SUCCESS,
          payload: data,
        });
      })
      .catch((err) => {
        dispatch({
          type: CREATE_ORDERS_ERROR,
          payload: err.response.data.errors,
        });
      });
  };
};
// export const changedValueUpdate = () => (dispatch) =>
//     dispatch({ type: DISPACH_UPDATING, payload: dispatch.payload });
