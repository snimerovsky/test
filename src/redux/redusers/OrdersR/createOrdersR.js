import {CREATE_ORDERS_ERROR, CREATE_ORDERS_REQUEST, CREATE_ORDERS_SUCCESS} from "../../actions/Orders/createOrders";
import {RESET_DEFAULT} from "../../actions/resetDefault";


const initialState = {
  createOrders: {},
  creating: true,
  errorMessage: "",
};

export const createOrdersR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ORDERS_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_ORDERS_SUCCESS:
      return {
        ...state,
        createOrders: action.payload,
        creating: false,
      };
    case CREATE_ORDERS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createOrders: {},
        creating: true,
      };
    default:
      return state;
  }
};
