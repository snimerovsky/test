import {
  GET_ORDERS_PRODUCTS_ERROR,
  GET_ORDERS_PRODUCTS_REQUEST,
  GET_ORDERS_PRODUCTS_SUCCESS,
} from "../../actions/Orders/getOrdersProducts";

const initialState = {
  OrdersProducts: {},
  OrdersProductsL: true,
};

export const getOrdersProductsR = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDERS_PRODUCTS_REQUEST:
      return {
        ...state,
        OrdersProductsL: true,
      };
    case GET_ORDERS_PRODUCTS_SUCCESS:
      return {
        ...state,
        OrdersProducts: action.payload,
        OrdersProductsL: false,
      };
    case GET_ORDERS_PRODUCTS_ERROR:
      return {
        ...state,
        OrdersProductsL: false,
      };
    default:
      return state;
  }
};
