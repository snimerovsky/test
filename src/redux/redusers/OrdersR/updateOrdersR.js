import { RESET_DEFAULT } from "../../actions/resetDefault";
import {UPDATE_ORDERS_ERROR, UPDATE_ORDERS_REQUEST, UPDATE_ORDERS_SUCCESS} from "../../actions/Orders/updateOrders";

const initialState = {
  updateOrders: {},
  updating: true,
  errorMessage: "",
};

export const updateOrdersR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_ORDERS_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_ORDERS_SUCCESS:
      return {
        ...state,
        updateOrders: action.payload,
        updating: false,
      };
    case UPDATE_ORDERS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateOrders: {},
        updating: true,
      };
    default:
      return state;
  }
};
