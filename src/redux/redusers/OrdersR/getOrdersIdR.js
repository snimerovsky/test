import {GET_ORDERS_ID_ERROR, GET_ORDERS_ID_REQUEST, GET_ORDERS_ID_SUCCESS} from "../../actions/Orders/getOrdersId";


const initialState = {
  StockId: {},
  OrdersIdL: true,
};

export const getOrdersIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDERS_ID_REQUEST:
      return {
        ...state,
        OrdersIdL: true,
      };
    case GET_ORDERS_ID_SUCCESS:
      return {
        ...state,
        StockId: action.payload,
        OrdersIdL: false,
      };
    case GET_ORDERS_ID_ERROR:
      return {
        ...state,
        OrdersIdL: false,
      };
    default:
      return state;
  }
};
