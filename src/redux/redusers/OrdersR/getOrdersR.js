import {GET_ORDERS_ERROR, GET_ORDERS_REQUEST, GET_ORDERS_SUCCESS} from "../../actions/Orders/getOrders";


const initialState = {
  Orders: {},
  OrdersL: true,
};

export const getOrdersR = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDERS_REQUEST:
      return {
        ...state,
        OrdersL: true,
      };
    case GET_ORDERS_SUCCESS:
      return {
        ...state,
        Orders: action.payload,
        OrdersL: false,
      };
    case GET_ORDERS_ERROR:
      return {
        ...state,
        OrdersL: false,
      };
    default:
      return state;
  }
};
