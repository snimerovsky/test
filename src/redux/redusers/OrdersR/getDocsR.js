import {GET_DOCS_ERROR, GET_DOCS_REQUEST, GET_DOCS_SUCCESS} from "../../actions/Orders/getDocs";


const initialState = {
    Docs: {},
    DocsL: true,
};

export const getDocsR = (state = initialState, action) => {
    switch (action.type) {
        case GET_DOCS_REQUEST:
            return {
                ...state,
                DocsL: true,
            };
        case GET_DOCS_SUCCESS:
            return {
                ...state,
                Docs: action.payload,
                DocsL: false,
            };
        case GET_DOCS_ERROR:
            return {
                ...state,
                DocsL: false,
            };
        default:
            return state;
    }
};
