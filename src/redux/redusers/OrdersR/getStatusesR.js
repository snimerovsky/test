import {GET_STATUSES_ERROR, GET_STATUSES_REQUEST, GET_STATUSES_SUCCESS} from "../../actions/Orders/getStatuses";


const initialState = {
    Statuses: {},
    StatusesL: true,
};

export const getStatusesR = (state = initialState, action) => {
    switch (action.type) {
        case GET_STATUSES_REQUEST:
            return {
                ...state,
                StatusesL: true,
            };
        case GET_STATUSES_SUCCESS:
            return {
                ...state,
                Statuses: action.payload,
                StatusesL: false,
            };
        case GET_STATUSES_ERROR:
            return {
                ...state,
                StatusesL: false,
            };
        default:
            return state;
    }
};
