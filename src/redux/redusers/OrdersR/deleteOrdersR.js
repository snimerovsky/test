import { RESET_DEFAULT } from "../../actions/resetDefault";
import {DELETE_ORDERS_ERROR, DELETE_ORDERS_REQUEST, DELETE_ORDERS_SUCCESS} from "../../actions/Orders/deleteOrders";


const initialState = {
  updateOrders: {},
  deleting: true,
  errorMessage: "",
};

export const deleteOrdersR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_ORDERS_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_ORDERS_SUCCESS:
      return {
        ...state,
        updateOrders: action.payload,
        deleting: false,
      };
    case DELETE_ORDERS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateOrders: {},
        deleting: true,
      };
    default:
      return state;
  }
};
