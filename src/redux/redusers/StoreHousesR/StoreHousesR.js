import {
    GET_STOREHOUSES_ERROR,
    GET_STOREHOUSES_REQUEST,
    GET_STOREHOUSES_SUCCESS
} from "../../actions/StoreHouses/getStoreHouses";

const initialState = {
  StoreHouses: {},
  StoreHousesL: true,
};

export const getStoreHousesR = (state = initialState, action) => {
  switch (action.type) {
    case GET_STOREHOUSES_REQUEST:
      return {
        ...state,
        StoreHousesL: true,
      };
    case GET_STOREHOUSES_SUCCESS:
      return {
        ...state,
        StoreHouses: action.payload,
        StoreHousesL: false,
      };
    case GET_STOREHOUSES_ERROR:
      return {
        ...state,
        StoreHousesL: false,
      };
    default:
      return state;
  }
};
