import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
  CREATE_STORE_HOUSES_ERROR,
  CREATE_STORE_HOUSES_REQUEST,
  CREATE_STORE_HOUSES_SUCCESS,
} from "../../actions/StoreHouses/createStoreHouses";

const initialState = {
  createStoreHouses: {},
  creating: true,
  errorMessage: "",
};

export const createStoreHousesR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STORE_HOUSES_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_STORE_HOUSES_SUCCESS:
      return {
        ...state,
        createStoreHouses: action.payload,
        creating: false,
      };
    case CREATE_STORE_HOUSES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createStoreHouses: {},
        creating: true,
      };
    default:
      return state;
  }
};
