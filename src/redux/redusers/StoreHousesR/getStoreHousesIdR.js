import {
  GET_STORE_HOUSES_ID_ERROR,
  GET_STORE_HOUSES_ID_REQUEST,
  GET_STORE_HOUSES_ID_SUCCESS,
} from "../../actions/StoreHouses/getStoreHousesId";

const initialState = {
  StoreHouse: {},
  StoreHouseL: true,
};

export const getStoreHousesIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_STORE_HOUSES_ID_REQUEST:
      return {
        ...state,
        StoreHouseL: true,
      };
    case GET_STORE_HOUSES_ID_SUCCESS:
      return {
        ...state,
        StoreHouse: action.payload,
        StoreHouseL: false,
      };
    case GET_STORE_HOUSES_ID_ERROR:
      return {
        ...state,
        StoreHouseL: false,
      };
    default:
      return state;
  }
};
