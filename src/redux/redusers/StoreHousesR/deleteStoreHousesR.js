import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
    DELETE_STORE_HOUSES_ERROR,
    DELETE_STORE_HOUSES_REQUEST,
    DELETE_STORE_HOUSES_SUCCESS
} from "../../actions/StoreHouses/deleteStoreHouses";

const initialState = {
  deleteStoreHouses: {},
  deleting: true,
  errorMessage: "",
};

export const deleteStoreHousesR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_STORE_HOUSES_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_STORE_HOUSES_SUCCESS:
      return {
        ...state,
        deleteStoreHouses: action.payload,
        deleting: false,
      };
    case DELETE_STORE_HOUSES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        deleteStoreHouses: {},
        deleting: true,
      };
    default:
      return state;
  }
};
