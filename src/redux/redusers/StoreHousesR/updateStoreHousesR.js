import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
    UPDATE_STORE_HOUSES_ERROR,
    UPDATE_STORE_HOUSES_REQUEST,
    UPDATE_STORE_HOUSES_SUCCESS
} from "../../actions/StoreHouses/updateStoreHouses";

const initialState = {
  updateStoreHouses: {},
  updating: true,
  errorMessage: "",
};

export const updateStoreHousesR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_STORE_HOUSES_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_STORE_HOUSES_SUCCESS:
      return {
        ...state,
        updateStoreHouses: action.payload,
        updating: false,
      };
    case UPDATE_STORE_HOUSES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateStoreHouses: {},
        updating: true,
      };
    default:
      return state;
  }
};
