import {
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
} from "../../auth/resetPassword";
import { RESET_DEFAULT } from "../../actions/resetDefault";

const initialState = {
  resetEmailData: {},
  isReset: false,
  errorMessage: "",
};

export const resetPasswordR = (state = initialState, action) => {
  switch (action.type) {
    case RESET_PASSWORD_REQUEST:
      return {
        ...state,
        rejectCreating: true,
      };
    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        resetEmailData: action.payload,
        isReset: true,
      };
    case RESET_PASSWORD_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        errorMessage: "",
        rejectCreating: false,
      };
    default:
      return state;
  }
};
