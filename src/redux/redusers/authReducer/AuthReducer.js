import {LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS} from "../../auth/login";
import {RESET_DEFAULT} from "../../actions/resetDefault";


const initialState = {
  userData: {},
  rejectCreating: true,
  isAuth: false,
  errorMessage: "",
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        rejectCreating: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        userData: action.payload,
        rejectCreating: false,
        isAuth: true,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
        rejectCreating: false,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        errorMessage: "",
        rejectCreating: false,
      };
    default:
      return state;
  }
};
