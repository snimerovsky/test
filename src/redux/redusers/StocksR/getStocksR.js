import {GET_STOCKS_ERROR, GET_STOCKS_REQUEST, GET_STOCKS_SUCCESS} from "../../actions/Stocks/getStocks";


const initialState = {
  Stocks: {},
  StocksL: true,
};

export const getStocksR = (state = initialState, action) => {
  switch (action.type) {
    case GET_STOCKS_REQUEST:
      return {
        ...state,
        StocksL: true,
      };
    case GET_STOCKS_SUCCESS:
      return {
        ...state,
        Stocks: action.payload,
        StocksL: false,
      };
    case GET_STOCKS_ERROR:
      return {
        ...state,
        StocksL: false,
      };
    default:
      return state;
  }
};
