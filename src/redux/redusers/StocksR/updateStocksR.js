import { RESET_DEFAULT } from "../../actions/resetDefault";
import {UPDATE_STOCKS_ERROR, UPDATE_STOCKS_REQUEST, UPDATE_STOCKS_SUCCESS} from "../../actions/Stocks/updateStocks";

const initialState = {
  updateStocks: {},
  updating: true,
  errorMessage: "",
};

export const updateStocksR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_STOCKS_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_STOCKS_SUCCESS:
      return {
        ...state,
        updateStocks: action.payload,
        updating: false,
      };
    case UPDATE_STOCKS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateStocks: {},
        updating: true,
      };
    default:
      return state;
  }
};
