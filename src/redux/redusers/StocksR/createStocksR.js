import { RESET_DEFAULT } from "../../actions/resetDefault";
import {CREATE_STOCKS_ERROR, CREATE_STOCKS_REQUEST, CREATE_STOCKS_SUCCESS} from "../../actions/Stocks/createStocks";



const initialState = {
  createStocks: {},
  creating: true,
  errorMessage: "",
};

export const createStocksR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_STOCKS_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_STOCKS_SUCCESS:
      return {
        ...state,
        createStocks: action.payload,
        creating: false,
      };
    case CREATE_STOCKS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createStocks: {},
        creating: true,
      };
    default:
      return state;
  }
};
