import { RESET_DEFAULT } from "../../actions/resetDefault";
import {DELETE_STOCKS_ERROR, DELETE_STOCKS_REQUEST, DELETE_STOCKS_SUCCESS} from "../../actions/Stocks/deleteStocks";


const initialState = {
  updateStocks: {},
  deleting: true,
  errorMessage: "",
};

export const deleteStocksR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_STOCKS_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_STOCKS_SUCCESS:
      return {
        ...state,
        updateStocks: action.payload,
        deleting: false,
      };
    case DELETE_STOCKS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateStocks: {},
        deleting: true,
      };
    default:
      return state;
  }
};
