import {
  GET_STOCKS_ID_ERROR,
  GET_STOCKS_ID_REQUEST,
  GET_STOCKS_ID_SUCCESS,
} from "../../actions/Stocks/getStockId";

const initialState = {
  StockId: {},
  StocksIdL: true,
};

export const getStockIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_STOCKS_ID_REQUEST:
      return {
        ...state,
        StocksIdL: true,
      };
    case GET_STOCKS_ID_SUCCESS:
      return {
        ...state,
        StockId: action.payload,
        StocksIdL: false,
      };
    case GET_STOCKS_ID_ERROR:
      return {
        ...state,
        StocksIdL: false,
      };
    default:
      return state;
  }
};
