import {GET_USERS_ID_ERROR, GET_USERS_ID_REQUEST, GET_USERS_ID_SUCCESS} from "../../actions/Users/getUserId";



const initialState = {
  user: {},
  userL: true,
};

export const getUserIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS_ID_REQUEST:
      return {
        ...state,
        userL: true,
      };
    case GET_USERS_ID_SUCCESS:
      return {
        ...state,
        user: action.payload,
        userL: false,
      };
    case GET_USERS_ID_ERROR:
      return {
        ...state,
        userL: false,
      };

    default:
      return state;
  }
};
