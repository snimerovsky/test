import {
  GET_USERS_ERROR,
  GET_USERS_REQUEST,
  GET_USERS_SUCCESS,
} from "../../actions/Users/getUsers";

const initialState = {
  users: {},
  usersL: true,
};

export const getUsersR = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS_REQUEST:
      return {
        ...state,
        usersL: true,
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        users: action.payload,
        usersL: false,
      };
    case GET_USERS_ERROR:
      return {
        ...state,
        usersL: false,
      };

    default:
      return state;
  }
};
