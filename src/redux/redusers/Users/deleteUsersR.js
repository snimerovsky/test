import {DELETE_USERS_ERROR, DELETE_USERS_REQUEST, DELETE_USERS_SUCCESS} from "../../actions/Users/deleteUsers";


const initialState = {
  user: {},
  deleting: true,
};

export const deleteUsersR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_USERS_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_USERS_SUCCESS:
      return {
        ...state,
        user: action.payload,
        deleting: false,
      };
    case DELETE_USERS_ERROR:
      return {
        ...state,
        deleting: false,
      };
    default:
      return state;
  }
};
