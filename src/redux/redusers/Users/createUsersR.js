import {
  CREATE_USERS_ERROR,
  CREATE_USERS_REQUEST,
  CREATE_USERS_SUCCESS,
} from "../../actions/Users/createUsers";
import { RESET_DEFAULT } from "../../actions/resetDefault";

const initialState = {
  createUsers: {},
  creating: true,
  errorMessage: "",
};

export const createUsersR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_USERS_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_USERS_SUCCESS:
      return {
        ...state,
        createUsers: action.payload,
        creating: false,
      };
    case CREATE_USERS_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createUsers: {},
        creating: true,
      };
    default:
      return state;
  }
};
