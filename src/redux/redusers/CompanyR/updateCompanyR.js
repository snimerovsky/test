import {
  UPDATE_COMPANY_ERROR,
  UPDATE_COMPANY_REQUEST,
  UPDATE_COMPANY_SUCCESS
} from "../../actions/Company/updateCompany";
import {RESET_DEFAULT} from "../../actions/resetDefault";


const initialState = {
  updateCompany: {},
  updating: true,
  errorMessage: "",
};

export const updateCompanyR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_COMPANY_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_COMPANY_SUCCESS:
      return {
        ...state,
        updateCompany: action.payload,
        updating: false,
      };
    case UPDATE_COMPANY_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateCompany: {},
        updating: true,
      };
    default:
      return state;
  }
};
