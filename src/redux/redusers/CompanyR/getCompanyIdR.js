import {
  GET_COMPANY_ID_ERROR,
  GET_COMPANY_ID_REQUEST,
  GET_COMPANY_ID_SUCCESS,
} from "../../actions/Company/getCompanyId";

const initialState = {
  company: {},
  companyL: true,
};

export const getCompanyIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY_ID_REQUEST:
      return {
        ...state,
        companyL: true,
      };
    case GET_COMPANY_ID_SUCCESS:
      return {
        ...state,
        company: action.payload,
        companyL: false,
      };
    case GET_COMPANY_ID_ERROR:
      return {
        ...state,
        companyL: false,
      };
    default:
      return state;
  }
};
