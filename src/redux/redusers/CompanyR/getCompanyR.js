import {GET_COMPANY_ERROR, GET_COMPANY_REQUEST, GET_COMPANY_SUCCESS} from "../../actions/Company/getCompany";


const initialState = {
  company: {},
  companyL: true,
};

export const getCompanyR = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY_REQUEST:
      return {
        ...state,
        companyL: true,
      };
    case GET_COMPANY_SUCCESS:
      return {
        ...state,
        company: action.payload,
        companyL: false,
      };
    case GET_COMPANY_ERROR:
      return {
        ...state,
        companyL: false,
      };
    default:
      return state;
  }
};
