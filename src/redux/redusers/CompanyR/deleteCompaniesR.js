import {
  DELETE_COMPANIES_ERROR,
  DELETE_COMPANIES_REQUEST,
  DELETE_COMPANIES_SUCCESS,
} from "../../actions/Company/deleteCompanies";

const initialState = {
  company: {},
  deleting: true,
};

export const deleteCompaniesR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_COMPANIES_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_COMPANIES_SUCCESS:
      return {
        ...state,
        company: action.payload,
        deleting: false,
      };
    case DELETE_COMPANIES_ERROR:
      return {
        ...state,
        deleting: false,
      };
    default:
      return state;
  }
};
