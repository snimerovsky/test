import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
    CREATE_COMPANIES_ERROR,
    CREATE_COMPANIES_REQUEST,
    CREATE_COMPANIES_SUCCESS
} from "../../actions/Company/createCompanies";

const initialState = {
  createCompanies: {},
  creating: true,
  errorMessage: "",
};

export const createCompaniesR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_COMPANIES_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_COMPANIES_SUCCESS:
      return {
        ...state,
        createCompanies: action.payload,
        creating: false,
      };
    case CREATE_COMPANIES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createCompanies: {},
        creating: true,
      };
    default:
      return state;
  }
};
