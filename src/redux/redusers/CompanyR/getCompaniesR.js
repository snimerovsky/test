import {
  GET_COMPANIES_ERROR,
  GET_COMPANIES_REQUEST,
  GET_COMPANIES_SUCCESS,
} from "../../actions/Company/getCompanies";

const initialState = {
  companies: {},
  companiesL: true,
};

export const getCompaniesR = (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANIES_REQUEST:
      return {
        ...state,
        companiesL: true,
      };
    case GET_COMPANIES_SUCCESS:
      return {
        ...state,
        companies: action.payload,
        companiesL: false,
      };
    case GET_COMPANIES_ERROR:
      return {
        ...state,
        companiesL: false,
      };
    default:
      return state;
  }
};
