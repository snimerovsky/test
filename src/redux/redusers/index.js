import { combineReducers } from "redux";
import { AuthReducer } from "./authReducer/AuthReducer";
import { resetPasswordR } from "./authReducer/resetPasswordR";
import { getProfileR } from "./ProfileR/getProfileR";
import { updateProfileR } from "./ProfileR/updateSourceR";
import { getCompanyR } from "./CompanyR/getCompanyR";
import { updateCompanyR } from "./CompanyR/updateCompanyR";
import { getCompaniesR } from "./CompanyR/getCompaniesR";
import { getUserIdR } from "./Users/getUserIdR";
import { getUsersR } from "./Users/getUsersR";
import { getCompanyIdR } from "./CompanyR/getCompanyIdR";
import { getStoreHousesR } from "./StoreHousesR/StoreHousesR";
import { getProductTypesR } from "./ProductTypesR/getProductTypesR";
import { getStoreHousesIdR } from "./StoreHousesR/getStoreHousesIdR";
import { updateStoreHousesR } from "./StoreHousesR/updateStoreHousesR";
import { getProductTypesIdR } from "./ProductTypesR/getProductTypesIdR";
import { updateProductTypesR } from "./ProductTypesR/updateProductTypesR";
import { deleteStoreHousesR } from "./StoreHousesR/deleteStoreHousesR";
import { deleteProductTypesR } from "./ProductTypesR/deleteProductTypesIdR";
import { createProductTypesR } from "./ProductTypesR/createProductTypesR";
import { createStoreHousesR } from "./StoreHousesR/createStoreHousesR";
import { deleteUsersR } from "./Users/deleteUsersR";
import { createUsersR } from "./Users/createUsersR";
import { deleteCompaniesR } from "./CompanyR/deleteCompaniesR";
import { createCompaniesR } from "./CompanyR/createCompaniesR";
import { updateStocksR } from "./StocksR/updateStocksR";
import { deleteStocksR } from "./StocksR/deleteStocksR";
import { createStocksR } from "./StocksR/createStocksR";
import { getStocksR } from "./StocksR/getStocksR";
import { getStockIdR } from "./StocksR/getStockIdR";
import { getSavedRowsR } from "./globalReduser";
import { getOrdersIdR } from "./OrdersR/getOrdersIdR";
import { updateOrdersR } from "./OrdersR/updateOrdersR";
import { getOrdersR } from "./OrdersR/getOrdersR";
import { deleteOrdersR } from "./OrdersR/deleteOrdersR";
import { createOrdersR } from "./OrdersR/createOrdersR";
import { getDocsR } from "./OrdersR/getDocsR";
import { getStatusesR } from "./OrdersR/getStatusesR";
import { getOrdersProductsR } from "./OrdersR/getOrdersProductsR";

const rootReducers = combineReducers({
  AuthReducer: AuthReducer,
  resetPasswordR: resetPasswordR,
  getProfileR: getProfileR,
  updateProfileR: updateProfileR,
  // Company
  getCompanyR: getCompanyR,
  updateCompanyR: updateCompanyR,
  getCompaniesR: getCompaniesR,
  deleteCompaniesR: deleteCompaniesR,
  createCompaniesR: createCompaniesR,
  // User
  getUsersR: getUsersR,
  deleteUsersR: deleteUsersR,
  createUsersR: createUsersR,
  getUserIdR: getUserIdR,
  getCompanyIdR: getCompanyIdR,
  // StoreHouses
  getStoreHousesR: getStoreHousesR,
  getStoreHousesIdR: getStoreHousesIdR,
  updateStoreHousesR: updateStoreHousesR,
  deleteStoreHousesR: deleteStoreHousesR,
  createStoreHousesR: createStoreHousesR,
  // ProductTypes
  getProductTypesIdR: getProductTypesIdR,
  updateProductTypesR: updateProductTypesR,
  getProductTypesR: getProductTypesR,
  deleteProductTypesR: deleteProductTypesR,
  createProductTypesR: createProductTypesR,
  // Stocks
  getStockIdR: getStockIdR,
  updateStocksR: updateStocksR,
  getStocksR: getStocksR,
  deleteStocksR: deleteStocksR,
  createStocksR: createStocksR,
  // Orders
  getOrdersIdR: getOrdersIdR,
  updateOrdersR: updateOrdersR,
  getOrdersR: getOrdersR,
  deleteOrdersR: deleteOrdersR,
  createOrdersR: createOrdersR,
  getSavedRowsR: getSavedRowsR,
  // Common
  getDocsR: getDocsR,
  getOrdersProductsR: getOrdersProductsR,
  getStatusesR: getStatusesR,
});

export default rootReducers;
