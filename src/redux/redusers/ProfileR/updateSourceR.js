import {
  UPDATE_PROFILE_ERROR,
  UPDATE_PROFILE_REQUEST,
  UPDATE_PROFILE_SUCCESS
} from "../../actions/Profile/updateProfile";
import {RESET_DEFAULT} from "../../actions/resetDefault";


const initialState = {
  updateProfile: {},
  updating: true,
  errorMessage: "",
};

export const updateProfileR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        updateProfile: action.payload,
        updating: false,
      };
    case UPDATE_PROFILE_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        user: {},
        updating: true,
      };
    default:
      return state;
  }
};
