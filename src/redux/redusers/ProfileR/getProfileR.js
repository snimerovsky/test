import {
  GET_PROFILE_ERROR,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
} from "../../actions/Profile/getProfile";

const initialState = {
  profile: {},
  profileL: true,
};

export const getProfileR = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE_REQUEST:
      return {
        ...state,
        profileL: true,
      };
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        profile: action.payload,
        profileL: false,
      };
    case GET_PROFILE_ERROR:
      return {
        ...state,
        profileL: false,
      };
    default:
      return state;
  }
};
