import {
  CREATE_SOURCE_ERROR,
  CREATE_SOURCE_REQUEST,
  CREATE_SOURCE_SUCCESS, DISPACH_CREATING,
} from "../../actions/Sourses/createSourse";

const initialState = {
  createdSource: {},
  creating: true,
  errorMessage: "",
};

export const createSourceR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SOURCE_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_SOURCE_SUCCESS:
      return {
        ...state,
        createdSource: action.payload,
        creating: false,
      };
    case CREATE_SOURCE_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case DISPACH_CREATING:
      return {
        ...state,
        creating: true,
        errorMessage: "",
      };
    default:
      return state;
  }
};
