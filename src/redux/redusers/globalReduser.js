import { SAVE_ROWS } from "../actions/saveRows";

const initialState = {
  SavedRows: [],
};

export const getSavedRowsR = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_ROWS:
      return {
        ...state,
        SavedRows: action.payload,
      };
    default:
      return state;
  }
};
