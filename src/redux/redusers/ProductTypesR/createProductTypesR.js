import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
  CREATE_PRODUCT_TYPES_ERROR,
  CREATE_PRODUCT_TYPES_REQUEST,
  CREATE_PRODUCT_TYPES_SUCCESS,
} from "../../actions/ProductTypes/createProductTypes";

const initialState = {
  createProductTypes: {},
  creating: true,
  errorMessage: "",
};

export const createProductTypesR = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PRODUCT_TYPES_REQUEST:
      return {
        ...state,
        creating: true,
      };
    case CREATE_PRODUCT_TYPES_SUCCESS:
      return {
        ...state,
        createProductTypes: action.payload,
        creating: false,
      };
    case CREATE_PRODUCT_TYPES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        createProductTypes: {},
        creating: true,
      };
    default:
      return state;
  }
};
