import {
  UPDATE_PRODUCT_TYPES_ERROR,
  UPDATE_PRODUCT_TYPES_REQUEST,
  UPDATE_PRODUCT_TYPES_SUCCESS,
} from "../../actions/ProductTypes/updateProductTypes";
import { RESET_DEFAULT } from "../../actions/resetDefault";

const initialState = {
  updateProductTypes: {},
  updating: true,
  errorMessage: "",
};

export const updateProductTypesR = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PRODUCT_TYPES_REQUEST:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_PRODUCT_TYPES_SUCCESS:
      return {
        ...state,
        updateProductTypes: action.payload,
        updating: false,
      };
    case UPDATE_PRODUCT_TYPES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateProductTypes: {},
        updating: true,
      };
    default:
      return state;
  }
};
