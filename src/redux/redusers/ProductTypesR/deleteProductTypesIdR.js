import { RESET_DEFAULT } from "../../actions/resetDefault";
import {
    DELETE_PRODUCT_TYPES_ERROR,
    DELETE_PRODUCT_TYPES_REQUEST,
    DELETE_PRODUCT_TYPES_SUCCESS
} from "../../actions/ProductTypes/deleteProductTypes";

const initialState = {
  updateProductTypes: {},
  deleting: true,
  errorMessage: "",
};

export const deleteProductTypesR = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_PRODUCT_TYPES_REQUEST:
      return {
        ...state,
        deleting: true,
      };
    case DELETE_PRODUCT_TYPES_SUCCESS:
      return {
        ...state,
        updateProductTypes: action.payload,
        deleting: false,
      };
    case DELETE_PRODUCT_TYPES_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
      };
    case RESET_DEFAULT:
      return {
        ...state,
        updateProductTypes: {},
        deleting: true,
      };
    default:
      return state;
  }
};
