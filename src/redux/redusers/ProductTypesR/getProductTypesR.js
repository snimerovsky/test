import {
  GET_PRODUCT_TYPES_ERROR,
  GET_PRODUCT_TYPES_REQUEST,
  GET_PRODUCT_TYPES_SUCCESS
} from "../../actions/ProductTypes/getProductTypes";


const initialState = {
  ProductTypes: {},
  ProductTypesL: true,
};

export const getProductTypesR = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_TYPES_REQUEST:
      return {
        ...state,
        ProductTypesL: true,
      };
    case GET_PRODUCT_TYPES_SUCCESS:
      return {
        ...state,
        ProductTypes: action.payload,
        ProductTypesL: false,
      };
    case GET_PRODUCT_TYPES_ERROR:
      return {
        ...state,
        ProductTypesL: false,
      };
    default:
      return state;
  }
};
