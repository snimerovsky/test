import {
  GET_PRODUCT_TYPES_ID_ERROR,
  GET_PRODUCT_TYPES_ID_REQUEST,
  GET_PRODUCT_TYPES_ID_SUCCESS
} from "../../actions/ProductTypes/getProductTypesId";


const initialState = {
  ProductType: {},
  ProductTypeL: true,
};

export const getProductTypesIdR = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_TYPES_ID_REQUEST:
      return {
        ...state,
        ProductTypeL: true,
      };
    case GET_PRODUCT_TYPES_ID_SUCCESS:
      return {
        ...state,
        ProductType: action.payload,
        ProductTypeL: false,
      };
    case GET_PRODUCT_TYPES_ID_ERROR:
      return {
        ...state,
        ProductTypeL: false,
      };
    default:
      return state;
  }
};
