import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import rootReducers from "./redusers";

const middlewares = [thunk];

middlewares.push(createLogger());

const store = createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware(...middlewares))
);

window.store = store;

export default store;
