export const baseURL = "https://api-dev.asso-rail.ru";
export const stocksURL = "https://api-dev.asso-rail.ru";
const setToken = () => {
  if (localStorage.userInfo === null || undefined) {
    return localStorage.setItem("userInfo", "");
  }
  return localStorage.userInfo;
};
export const axiosConfig = {
  headers: {
    Accept: "application/json",
    Authorization: "Bearer " + setToken(),
  },
};
