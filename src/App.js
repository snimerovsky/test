import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LayoutContainer from "./components/LayoutContainer/LayoutContainer";
import Login from "./pages/Login";
import ResetPassword from "./pages/ResetPassword";
import PrivateRoute from "./pages/PrivateRoute";
import LogedRoute from "./pages/LogedRoute";
// import AuthLogin from "./pages/AuthLogin";

const HomeRoute = lazy(() => import("./pages/Home.jsx"));
const CompanyRoute = lazy(() => import("./pages/Company.jsx"));
const StoreHousesRoute = lazy(() =>
  import("./components/StoreHouses/StoreHouses.jsx")
);
const ProductTypesRoute = lazy(() =>
  import("./components/ProductTypes/ProductTypes.jsx")
);
const CreateProductTypesRoute = lazy(() =>
  import("./components/ProductTypes/CreateProductTypes.jsx")
);
const CompaniesRoute = lazy(() =>
  import("./components/Companies/Companies.jsx")
);
const EditCompanies = lazy(() =>
  import("./components/Companies/EditCompanies.jsx")
);
const UsersRoute = lazy(() => import("./components/Users/Users.jsx"));
const EditUsers = lazy(() => import("./components/Users/EditUsers.jsx"));
const CreateStoreHousesRoute = lazy(() =>
  import("./components/StoreHouses/CreateStoreHouses.jsx")
);
const CreateCompaniesRoute = lazy(() =>
  import("./components/Companies/CreateCompanies.jsx")
);
const CreateUsersRoute = lazy(() =>
  import("./components/Users/CreateUsers.jsx")
);
const CreateStocksRoute = lazy(() =>
  import("./components/Stocks/CreateStocks.jsx")
);
const CreateOrdersRoute = lazy(() =>
  import("./components/OrdersTable/OrderCreate")
);
const EditStoreHousesRoute = lazy(() =>
  import("./components/StoreHouses/EditStoreHouses.jsx")
);
const EditProductTypesRoute = lazy(() =>
  import("./components/ProductTypes/EditProductTypes.jsx")
);
const EditStocksRoute = lazy(() =>
  import("./components/Stocks/EditStocks.jsx")
);
const EditOrdersRoute = lazy(() =>
  import("./components/OrdersTable/EditOrder.jsx")
);
const StocksRoute = lazy(() => import("./components/Stocks/Stocks.jsx"));
const DetailRoute = lazy(() => import("./pages/DetailPage.jsx"));
const OrdersRoute = lazy(() => import("./pages/Orders.jsx"));
const NotFoundRoute = lazy(() => import("./pages/NotFoundRoute"));

function App() {
  return (
    <Router>
      <Switch>
        <LogedRoute exact path="/" component={Login} />
        <LogedRoute path="/reset" component={ResetPassword} />
        <LayoutContainer>
          <Suspense fallback={<div></div>}>
            <Switch>
              <PrivateRoute exact path="/profile" component={HomeRoute} />
              <PrivateRoute
                exact
                path="/storehouses"
                component={StoreHousesRoute}
              />
              <PrivateRoute
                exact
                path="/createStoreHouses"
                component={CreateStoreHousesRoute}
              />
              <PrivateRoute
                path="/storehousesedit/:id"
                component={EditStoreHousesRoute}
              />
              <PrivateRoute
                exact
                path="/product-types"
                component={ProductTypesRoute}
              />
              <PrivateRoute
                exact
                path="/createProductTypes"
                component={CreateProductTypesRoute}
              />
              <PrivateRoute
                path="/producttypesedit/:id"
                component={EditProductTypesRoute}
              />
              <PrivateRoute exact path="/stocks" component={StocksRoute} />
              <PrivateRoute
                exact
                path="/createStocks"
                component={CreateStocksRoute}
              />
              <PrivateRoute
                path="/stocksEdit/:id"
                component={EditStocksRoute}
              />
              <PrivateRoute exact path="/company" component={CompanyRoute} />
              <PrivateRoute
                exact
                path="/companies"
                component={CompaniesRoute}
              />
              <PrivateRoute
                exact
                path="/createCompanies"
                component={CreateCompaniesRoute}
              />
              <PrivateRoute
                path="/companiesedit/:id"
                component={EditCompanies}
              />
              <PrivateRoute exact path="/users" component={UsersRoute} />
              <PrivateRoute
                exact
                path="/createUsers"
                component={CreateUsersRoute}
              />
              <PrivateRoute path="/usersedit/:id" component={EditUsers} />
              <PrivateRoute exact path="/tickets" component={OrdersRoute} />
              <PrivateRoute
                exact
                path="/createTicket"
                component={CreateOrdersRoute}
              />
              <PrivateRoute
                path="/OrdersEdit/:id"
                component={EditOrdersRoute}
              />
              <PrivateRoute exact path="/tickets" component={OrdersRoute} />
              <PrivateRoute exact path="/detailpage" component={DetailRoute} />
              <Route component={NotFoundRoute} />
            </Switch>
          </Suspense>
        </LayoutContainer>
      </Switch>
    </Router>
  );
}

export default App;
